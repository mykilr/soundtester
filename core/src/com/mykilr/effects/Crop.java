package com.mykilr.effects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Slider;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.ShortArray;
import com.mykilr.tools.PCMData;
import com.mykilr.ui.ZoomableSample;

public class Crop extends Effect {

	private static final int AUTOCROP_AMPLITUDE = Short.MAX_VALUE / 10;
	private static final String effectName = "Crop";
	
	private ShortArray cropDisplay;
	private PCMData outputSample;
	private Table table;
	private ZoomableSample sampleView;
	// TODO: Sliders are probably not the best way to do this
	private Slider startSlider;
	private Slider endSlider;
	
	public Crop(Skin skin) {
		cropDisplay = new ShortArray();
		outputSample = new PCMData();
		table = new Table(skin);
		sampleView = new ZoomableSample(cropDisplay, skin);
		startSlider = new Slider(0, 1, 1, false, skin);
		endSlider = new Slider(0, 1, 1, false, skin);
		endSlider.setValue(1);
		startSlider.addListener(sliderListener);
		endSlider.addListener(sliderListener);
		
		sampleView.markers.add(0);
		sampleView.markers.add(cropDisplay.size - 1);
		startSlider.setWidth(sampleView.getDrawWidth());
		endSlider.setWidth(sampleView.getDrawWidth());
		
		table.add(startSlider).align(Align.left).width(sampleView.getDrawWidth());
		table.row();
		table.add(sampleView);
		table.row();
		table.add(endSlider).align(Align.right).width(sampleView.getDrawWidth());
		table.row();
		table.add().height(160);
	}
	
	@Override
	public Table getGui() {
		Gdx.app.log("Crop","getGui");
		sampleView.setSampleData(cropDisplay);
		return table;
	}

	@Override
	public void random() {
	}

	@Override
	public void setSample(PCMData sample) {
		Gdx.app.log("Crop","setSample, ess=" + outputSample.sample.size + ", nss=" + sample.sample.size);
		
		boolean sampleLengthChanged = 
				(sample.sample.size != cropDisplay.size);
	
		outputSample.set(sample);
		cropDisplay.clear();
		cropDisplay.addAll(sample.sample);
		
		updateMarkersAndSliders(sampleLengthChanged);
	}
	
	private void updateMarkersAndSliders(boolean sampleLengthChanged) {
		Gdx.app.log("Crop","updateMarkersAndSliders, lengthChange: " + sampleLengthChanged);
		
		if (sampleLengthChanged) {
			startSlider.setValue(0);
			// reset the crop markers
			if (outputSample.sample.size > 0) {
				startSlider.setRange(0, outputSample.sample.size - 1);
				endSlider.setRange(0, outputSample.sample.size - 1);
				endSlider.setValue(outputSample.sample.size - 1);
			} else {
				startSlider.setRange(0, 1);
				endSlider.setRange(0, 1);
				endSlider.setValue(1);
			}
			
			autoCrop();
		}
		
		updateMarkersFromSliders();
		
	}
	
	private void autoCrop() {
		int i = 0;
		while(i < cropDisplay.size && Math.abs(cropDisplay.get(i)) < AUTOCROP_AMPLITUDE) i++;
		startSlider.setValue(i);
		int j = cropDisplay.size -1;
		while(j > i && Math.abs(cropDisplay.get(j)) < AUTOCROP_AMPLITUDE) j--;
		endSlider.setValue(j);
	}

	private void updateMarkersFromSliders() {
		if (startSlider.getValue() > endSlider.getValue()) {
			float tmp = endSlider.getValue();
			endSlider.setValue(startSlider.getValue());
			startSlider.setValue(tmp);
		}
		
		sampleView.markers.items[0] = (int) startSlider.getValue();
		sampleView.markers.items[1] = (int) endSlider.getValue();
	}
	
	ChangeListener sliderListener = new ChangeListener() {
		@Override
		public void changed(ChangeEvent event, Actor actor) {
			updateMarkersFromSliders();
		}
	};

	@Override
	public PCMData update() {
		Gdx.app.log("Crop","update");
		outputSample.sample.clear();
		outputSample.sample.addAll(cropDisplay);
		crop(outputSample, (int)startSlider.getValue(), (int)endSlider.getValue());
		return outputSample;
	}

	@Override
	public PCMData update(PCMData stageInput) {
		setSample(stageInput);
		return update();
	}

	@Override
	public boolean hasGui() {
		return table != null;
	}
	
	@Override
	public String toString() {
		return effectName;
	}
	
	private static void crop(PCMData sample, int start, int end){
		if (start < 0 || start > end || end >= sample.sample.size) return;

		sample.sample.removeRange(end, sample.sample.size - 1);
		sample.sample.removeRange(0, start);
		
	}



}
