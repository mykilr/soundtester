package com.mykilr.effects;

import java.util.Random;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Event;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Slider;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.FloatArray;
import com.badlogic.gdx.utils.ShortArray;
import com.mykilr.tools.PCMData;
import com.mykilr.tools.PCMDomain;
import com.mykilr.tools.Transform;
import com.mykilr.ui.EditGraph;

public class Equaliser extends Effect {

	private static final String effectName = "Equalizer";
	private static final float[] bandLimits = {22, 45, 90, 179, 353, 706, 1401, 2803, 5605, 11210, 22421};
	private static final String[] bandLabels = {
		" 16 ", " 31 ", " 63 ", " 125", " 250", " 500", " 1k ", 
		" 2k ", " 4k ", " 8k ", " 16k"};
	private static final float[] bandMids = {16, 31.25f, 62.5f, 125, 250, 500, 1000, 2000, 4000, 8000, 16000};
	
	private static final int BANDS = bandLimits.length;
	private static Random rand = new Random(System.currentTimeMillis());
	
	private Label[] eqLabels;
	private Slider[] eqSliders;
	private Table table;
	private PCMData outputSample;
	private PCMDomain dft;
	
	public Equaliser(Skin skin) {
		table = new Table(skin);
		outputSample = new PCMData();
		dft = new PCMDomain();
		eqLabels = new Label[BANDS];
		eqSliders = new Slider[BANDS];
		// Give the EQ some labels
		for (int i = 0; i < BANDS; i++) {
			eqLabels[i] = new Label(String.valueOf(bandLabels[i]), skin);
			table.add(eqLabels[i]);
		}
		
		table.row();
		// Setup a mid pass curve
		for (int i = 0; i < BANDS; i++) {
			eqSliders[i] = new Slider(0, 1, (1f / Short.MAX_VALUE), true, skin);
			eqSliders[i].setValue((float) (1f - ( Math.pow((2 * i * (1f / BANDS) - 1),2))));
//			eqSliders[i].addListener(new DraggedListener(eqSliders[i]));
			table.add(eqSliders[i]);
		}
		table.row();
	}
	
	@Override
	public Table getGui() {
		
		return table;
	}
	
//	private class DraggedListener extends InputListener {
//		
//		private Slider slider;
//
//		public DraggedListener(Slider slider) {
//			this.slider = slider;
//		}
//
//		@Override
//		public void exit(InputEvent event, float x, float y, int pointer, Actor toActor) {
//			if (pointer != -1) {
//				Gdx.app.log("EQDrag", "event: " + event + ", x: " + x + ", y: " + y
//						+ ", pointer: " + pointer + ", fromActor: " + toActor);
//				event.setType(InputEvent.Type.touchUp);
//				slider.fire(event);
//			}
//		};
//		
//		@Override
//		public void enter(InputEvent event, float x, float y, int pointer, Actor fromActor) {
//			if (pointer != -1) {
//				Gdx.app.log("EQDrag", "event: " + event + ", x: " + x + ", y: " + y
//						+ ", pointer: " + pointer + ", fromActor: " + fromActor);
//				event.setType(InputEvent.Type.touchDown);
//				slider.fire(event);
//			}
//		};
//		
//		@Override
//		public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
//			Gdx.app.log("EQDrag", "event: " + event + ", x: " + x + ", y: " + y
//					+ ", pointer: " + pointer + ", button: " + button);
//		};
//		
//		@Override
//		public boolean touchDown(InputEvent event, float x, float y,
//				int pointer, int button) {
//			Gdx.app.log("EQDrag", "event: " + event + ", x: " + x + ", y: " + y
//					+ ", pointer: " + pointer + ", button: " + button);
//			return super.touchDown(event, x, y, pointer, button);
//		}
//		
//		
//	}
	
	

	@Override
	public void random() {
		for (int i = 0; i < BANDS; i++) {
			eqSliders[i].setValue(rand.nextInt(Short.MAX_VALUE));
		}
	}

	@Override
	public void setSample(PCMData sample) {
		outputSample.set(sample);
	}

	@Override
	public PCMData update() {
		FloatArray freqGraph = new FloatArray();
		for (int i = 0; i < BANDS; i++) {
			freqGraph.add(eqSliders[i].getValue());
		}
		
		applyEqualisationEnvelope(outputSample, dft, freqGraph);
		return outputSample;
	}

	@Override
	public PCMData update(PCMData stageInput) {
		setSample(stageInput);
		return update();
	}

	@Override
	public boolean hasGui() {
		return table != null;
	}
	
	@Override
	public String toString() {
		return effectName;
	}
	
	private static void applyEqualisationEnvelope(PCMData sample, PCMDomain dft, FloatArray freqGraph) {
		if (sample == null || sample.sample == null || sample.sample.size == 0) return;
		
		// Convert to Frequency domain
		int snipSize = Transform.copyShortSampleToFloatDomain(sample, dft);
		Transform.fastFourierTransform(dft, 0, snipSize, false);
		
		// Get the polar coords
		dft.updatePolarFromRectangular();
		
		// Create amplitude graph to apply to freq domain (mirrored)
		Array<Vector2> freqPoints = new  Array<Vector2>();
		float freqsPerDatum = (float)sample.sampleRate / (float)snipSize;
		// Add point at front edge, set equal to first limit
		float x = 0;
		float y = freqGraph.get(0);
		Gdx.app.log("EQ", "root: (" + x + "," + y + "), mag.size: " + 
				dft.magnitude.size + ", rate: " + sample.sampleRate + 
				", snipSize: " + snipSize);
		freqPoints.add(new Vector2(x, y));
		// Add all the mid points
		for (int i = 0; i < BANDS; i++) {
			x = bandMids[i] / (dft.magnitude.size * freqsPerDatum);
			y = freqGraph.get(i);
			Gdx.app.log("EQ", "" + freqPoints.size + ": (" + x + "," + y + ")");
			freqPoints.add(new Vector2(x, y));
		}
		
		// Duplicate the points in reverse order, but increasing x
		// There are BANDS + 1 elements already inserted
		for (int i = BANDS; i >= 0; i--) {
			x = 1.0f - freqPoints.get(i).x;
			y = freqPoints.get(i).y;
			Gdx.app.log("EQ", "" + freqPoints.size + ": (" + x + "," + y + ")");
			freqPoints.add(new Vector2(x, y));
		}
		
		
		// Apply amplitude graph to the freq domain
		Transform.applyAmplitudeEnvelope(dft.magnitude, freqPoints);
		
		// Update rectangular from polr coords
		dft.updateRectangularFromPolar();
		
		// Convert back to time domain
		float scaleRe = Transform.fastFourierTransform(dft, 0, snipSize, true);
		
		Transform.copyFloatDomainToShortSample(dft, sample, scaleRe);
	}
}
