package com.mykilr.effects;

import java.util.Random;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ShortArray;
import com.mykilr.tools.PCMData;
import com.mykilr.ui.EditGraph;

public class AmplitudeEnvelope extends Effect {
	
	/* Can I get away with this? I think not */
	private static String effectName = "ADSR Envelope";
	private static Random rand = new Random(System.currentTimeMillis());

	private EditGraph editGraph;
	private Table table;
	private PCMData outputSample;

	public AmplitudeEnvelope(final Skin skin) {
		table = new Table(skin);
		editGraph = new EditGraph(skin);
		outputSample = new PCMData();
		
		table.add(new Label("Amplitude", skin));
		table.add(editGraph).colspan(3);
		table.row();
		table.add();
		table.add(new Label("Time", skin)). colspan(3);
		table.row();
	}

	public Table getGui() {
		return table;
	}

	public void random() {
		editGraph.randomRepositioning(rand);
	}

	public void setSample(PCMData sample) {
		this.outputSample.set(sample);
	}
	
	public PCMData update() {
		applyAmplitudeEnvelope(outputSample.sample, editGraph.getPoints());
		return outputSample;
	}
	
	public PCMData update(PCMData sample) {
		setSample(sample);
		return update();
	}

	@Override
	public String toString() {
		return effectName;
	}

	@Override
	public boolean hasGui() {
		return (table != null);
	}

	/** Modifies the given sample's amplitude by the graph described by points.
	 * Each vector in the point represents a time (x) and an amplitude (y) where
	 * x is 0.0f to 1.0f from the start to the end of the sample, and 
	 * y is 0.0f to 1.0f representing silence to the loudest amplitude.
	 * 
	 * @param sample
	 * @param points
	 * @param sampleRate
	 */
	public static void applyAmplitudeEnvelope(ShortArray sample, Array<Vector2> points) {
		int partLen = 0;
		float amp = 0.0f; // Starting amplitude
		float ampD = 0.0f;
		int t0 = 0; // Sample time of first graph point
		
		for (int part = 0; part < points.size; part++) {
			// partLen is the distance between 2 graph points in time samples
			partLen = (int) Math.floor(sample.size * points.get(part).x) - t0;
			// ampD is the amount the amplitude changes per sampleDatum
			ampD = partLen > 0 ? (points.get(part).y - amp) / partLen : 0;
			// change the amplitude for all the data between the 2 graph points
			for (int t = 0; t < partLen; t++) {
				// modify the amplitude for each datum
				sample.set(t0 + t, (short) (sample.get(t0 + t) * (amp + ampD * t)));
			}
			// Move the time up to the next slope
			t0 += partLen;
			// Move the base amplitude to that of the next starting point
			amp = points.get(part).y;
		}
		// If there is still sample time left, do a full drop to silence
		partLen = sample.size - t0;
		// The last ampD should bring the data to zero
		ampD = partLen > 0 ? (0.0f - amp) / partLen : 0;
		for (int t = 0; t < partLen; t++) {
			sample.set(t0 + t, (short) (sample.get(t0 + t) * (amp + ampD * t)));
		}
	}
}
