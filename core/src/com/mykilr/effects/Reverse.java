package com.mykilr.effects;

import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.ShortArray;
import com.mykilr.tools.PCMData;

public class Reverse extends Effect {

	/* The scalar error is applied when converting from short to float and float to short,
	 * It appears that the scaling error helps remove artifacts from the final back/forth conversion.
	 * Don't know why, but works. */

	private static final String effectName = "Reverse";

	private PCMData outputSample;
	
	public Reverse(Skin skin) {
		outputSample = new PCMData();
	}
	
	@Override
	public Table getGui() {
		return null;
	}

	@Override
	public void random() {
	}

	@Override
	public void setSample(PCMData sample) {
		outputSample.set(sample);
	}

	@Override
	public PCMData update() {
		performExperiment01(outputSample);
		return outputSample;
	}

	@Override
	public PCMData update(PCMData stageInput) {
		setSample(stageInput);
		return update();
	}

	@Override
	public boolean hasGui() {
		return false;
	}
	
	@Override
	public String toString() {
		return effectName;
	}
	
	private static void performExperiment01(PCMData sample){
		reverseSample(sample.sample);
	}
	
	private static void reverseSample(ShortArray sample) {
		for (int i = 0; i < sample.size / 2; i++) {
			int i2 = sample.size - 1 - i;
			short temp = sample.items[i];
			sample.items[i] = sample.items[i2];
			sample.items[i2] = temp;
		}
	}

}
