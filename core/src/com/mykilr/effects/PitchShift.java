package com.mykilr.effects;

import java.util.Random;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Slider;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.mykilr.tools.PCMData;
import com.mykilr.tools.PCMDomain;
import com.mykilr.tools.Transform;
import com.mykilr.ui.CrossUpdateListener;

public class PitchShift extends Effect {

	private static final float MIN_PITCH_INC = -80;
	private static final float MAX_PITCH_INC = 80;
	private static final float PITCH_STEP = 1;
	private static final float PITCH_INC_DEFAULT = 20;
	private static final String effectName = "Pitch Adjust";

	private static Random rand = new Random(System.currentTimeMillis());

	private Slider editBar;
	private TextField editField;
	private Table table;
	private PCMData outputSample;
	private static PCMDomain dft;
	private Label freqLabel;
	
	public PitchShift(Skin skin) {
		outputSample = new PCMData();
		dft = new PCMDomain();
		table = new Table(skin);
		// TODO: Make the pitch shift values into real numbers (Hz, maybe?)
		editBar = new Slider(MIN_PITCH_INC, MAX_PITCH_INC, PITCH_STEP, false, skin);
		editBar.setValue(PITCH_INC_DEFAULT);
		editField = new TextField(String.valueOf(editBar.getValue()), skin);
		
		editBar.addListener(new FreqDiffUpdateListener(editBar));
		editBar.addListener(new CrossUpdateListener(editBar, editField));
		editField.setTextFieldListener(new CrossUpdateListener(editField, editBar));
		
		table.add(new Label("Modify Pitch: ", skin));
		freqLabel = new Label("", skin);
		table.add(freqLabel);
		table.row();
		table.add(editBar);
		table.add(editField);
		table.row();
		table.add().height(160);
	}
	
	@Override
	public Table getGui() {
		Gdx.app.log("PS get", outputSample.sampleRate + "/" + dft.real.size);
		return table;
	}

	@Override
	public void random() {
		float range = (MAX_PITCH_INC - MIN_PITCH_INC) / PITCH_STEP;
		float newGain = rand.nextFloat() * range * PITCH_STEP + MIN_PITCH_INC;
		editBar.setValue(newGain);
	}

	@Override
	public void setSample(PCMData sample) {
		outputSample.set(sample);
		Transform.copyShortSampleToFloatDomain(sample, dft);
		Transform.fastFourierTransform(dft, 0, dft.real.size, false);
		Gdx.app.log("PS set", outputSample.sample.size + "/" + dft.real.size);
	}

	@Override
	public PCMData update() {
		performPitchShift(outputSample, (int) editBar.getValue());
		return outputSample;
	}

	@Override
	public PCMData update(PCMData stageInput) {
		setSample(stageInput);
		return update();
	}

	@Override
	public boolean hasGui() {
		return true;
	}
	
	@Override
	public String toString() {
		return effectName;
	}
	

	public class FreqDiffUpdateListener extends ChangeListener {

		private Slider editBar;

		public FreqDiffUpdateListener(Slider editBar) {
			this.editBar = editBar;
		}

		@Override
		public void changed(ChangeEvent event, Actor actor) {
			// Update the freq label to indicate frequency adjustment
			Gdx.app.log("PS", outputSample.sampleRate + "/" + dft.real.size);
			float freqAdjust = (float)(outputSample.sampleRate) / dft.real.size * editBar.getValue();
			Gdx.app.log("PS", "=" + freqAdjust);
			freqLabel.setText(String.format("%.2f", freqAdjust) + "Hz");
		}

	}
	
	private static void performPitchShift(PCMData sample, int pitchShift){

		
		Transform.pitchShift(dft, pitchShift);
		
		float scale = Transform.fastFourierTransform(dft, 0, dft.real.size, true);
		
		Transform.copyFloatDomainToShortSample(dft, sample, scale);
	}
	


}
