package com.mykilr.effects;

import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.FloatArray;
import com.mykilr.tools.PCMData;
import com.mykilr.tools.Transform;

public class Experimental01 extends Effect {

	private static final String effectName = "Experiment 01";
	
	private PCMData outputSample;
	private Table table;
	
	public Experimental01(Skin skin) {
		outputSample = new PCMData();
	}
	
	@Override
	public Table getGui() {
		return table;
	}

	@Override
	public void random() {
	}

	@Override
	public void setSample(PCMData sample) {
		outputSample.set(sample);
	}

	@Override
	public PCMData update() {
		exp01(outputSample);
		return outputSample;
	}

	@Override
	public PCMData update(PCMData stageInput) {
		setSample(stageInput);
		return update();
	}

	@Override
	public boolean hasGui() {
		return table != null;
	}
	
	@Override
	public String toString() {
		return effectName;
	}
	
	private static void exp01(PCMData sample){
		FloatArray echoArray = new FloatArray();
		
		// Annoying copy from short to float
		for (int i = 0; i < sample.sample.size; i++) {
			echoArray.add(sample.sample.get(i));
		}
		
		float convertScale = createEcho(echoArray, 11025, 0.5f);
		
		Transform.copyFloatDomainToShortSample(echoArray, sample, convertScale, 0, echoArray.size, 0);
	}
	
	/**
	 * Copy sample data left or right
	 * @param data
	 * @param offset
	 */
	private static float createEcho(FloatArray data, int offset, float scale) {
		float maxAbsValue = 0.0f;
		// Increase sample size by echo length
		FloatArray origData = new FloatArray(data);
		int origDataSize = data.size;
		
		data.ensureCapacity(offset);
		for (int i = 0; i < offset; i++) {
			data.add(0.0f);
		}
		
		// If the offset is negative (pre-echo?) 
		if (offset < 0) {
			// move the sample forward
			for (int i = origDataSize + offset - 1; i >= offset; i--) {
				float value = data.get(i + offset);
				maxAbsValue = maxAbs(maxAbsValue, value);
				data.set(i, value);
			}
			for (int i = offset - 1; i >= 0; i--) {
				data.set(i, 0.0f);
			}
			// and copy towards the start of the array
			for (int i = origDataSize - 1; i >= 0; i--) {
				float value = data.get(i) + origData.get(i) * scale;
				maxAbsValue = maxAbs(maxAbsValue, value);
				data.set(i, value);
			}
		// otherwise, if the offset is positive
		} else if (offset > 0) {
			// Update maxAbsValue from the existing samples
			for (int i = 0; i < offset; i++) {
				float value = data.get(i);
				maxAbsValue = maxAbs(maxAbsValue, value);
			}
			// copy towards the end of the array
			for (int i = offset; i < origDataSize + offset; i++) {
				float value = data.get(i) + origData.get((i - offset)) * scale;
				maxAbsValue = maxAbs(maxAbsValue, value);
				data.set(i, value);
			}
		}
		return maxAbsValue;
	}
	
	private static float maxAbs(float a, float b) {
		return Math.max(Math.abs(a), Math.abs(b));
	}


}
