package com.mykilr.effects;

import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.mykilr.tools.PCMData;

public abstract class Effect {
	
	private static String effectName = "Unnamed Effect";
	
	Effect(){
		
	}
	
	public Effect(final Skin skin) {}
		
	public abstract boolean hasGui();
	
	public abstract Table getGui();

	public abstract void random();

	public abstract void setSample(PCMData sample);

	public abstract PCMData update();

	public abstract PCMData update(PCMData stageInput);
	
	@Override
	public String toString() {
		return effectName;
	}
}
