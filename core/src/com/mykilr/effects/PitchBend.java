package com.mykilr.effects;

import java.util.Random;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.SelectBox;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Slider;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.FloatArray;
import com.mykilr.tools.PCMData;
import com.mykilr.tools.PCMDomain;
import com.mykilr.tools.Transform;

public class PitchBend extends Effect {

	/* The scalar error is applied when converting from short to float and float to short,
	 * It appears that the scaling error helps remove artifacts from the final back/forth conversion.
	 * Don't know why, but works. */

	private static final String effectName = "Pitch Bend";

	private static final float MIN_PITCH_INC = -1;
	private static final float MAX_PITCH_INC = 2;
	private static final float PITCH_STEP = 1;
	private static final float PITCH_INC_DEFAULT = 1;
	private static final String PITCH_FORMAT = " %+2d";

	private Random rand;
	private PCMData outputSample;
	private Table table;
	private Slider editBar;
	private SelectBox<Integer> windowSelectBox;
	private Integer[] windowSizes = new Integer[] {
		128,
		256,
		512,
		1024,
		2048,
		4096
	};

	private Label shiftLabel;
	
	public PitchBend(Skin skin) {
		outputSample = new PCMData();
		rand = new Random(System.currentTimeMillis());

		table = new Table(skin);
		editBar = new Slider(MIN_PITCH_INC, MAX_PITCH_INC, PITCH_STEP, false, skin);
		editBar.setValue(PITCH_INC_DEFAULT);
		shiftLabel = new Label(String.format(PITCH_FORMAT,(int)editBar.getValue()), skin);
		windowSelectBox = new SelectBox<Integer>(skin);
		windowSelectBox.setItems(windowSizes);
		windowSelectBox.setSelectedIndex(3);
		
		editBar.addListener(new ChangeListener() {

			@Override
			public void changed(ChangeEvent event, Actor actor) {
				shiftLabel.setText(String.format(PITCH_FORMAT,(int)editBar.getValue()));
			}
			
		});
		
		table.add(new Label("Sample Shift Per Window: ", skin)).align(Align.right);
		table.add(editBar);
		table.add(shiftLabel);
		table.row();
		table.add(new Label("Shift Window Size:", skin)).align(Align.right);
		table.add(windowSelectBox).colspan(2).fill();
		table.row();
		table.add().height(160);
	}
	
	@Override
	public Table getGui() {
		return table;
	}

	@Override
	public void random() {
		float pitchRange = (MAX_PITCH_INC - MIN_PITCH_INC) / PITCH_STEP;
		float newTime = rand.nextFloat() * pitchRange * PITCH_STEP + MIN_PITCH_INC;
		editBar.setValue(newTime);
		windowSelectBox.setSelectedIndex(rand.nextInt(windowSelectBox.getItems().size));
	}

	@Override
	public void setSample(PCMData sample) {
		outputSample.set(sample);
	}

	@Override
	public PCMData update() {
		int windowSize = windowSelectBox.getSelected();
		int bendShift = (int) editBar.getValue();
		// Pitch bending on a sample smaller than the window is pointless
		if (outputSample != null && outputSample.sample.size > windowSize) {
			pitchBend(outputSample, windowSize, bendShift);
		}
		return outputSample;
	}

	@Override
	public PCMData update(PCMData stageInput) {
		setSample(stageInput);
		return update();
	}

	@Override
	public boolean hasGui() {
		return true;
	}
	
	@Override
	public String toString() {
		return effectName;
	}
	
	private static void pitchBend(PCMData sample, int windowSize, int bendShift){
		
		FloatArray combinedDft = new FloatArray();
		PCMDomain dftWindow = new PCMDomain();
		PCMData origInput = new PCMData();
		origInput.set(sample);
		
		int n = windowSize;
		int stages = (int) Math.ceil((double)origInput.sample.size / (double)windowSize);

		
		
		/* Extract and scale the even positioned windows */
		for (int i = 0; i < stages; i++) {
			// Extract short sections of time domain
			n = Math.min(windowSize, origInput.sample.size - i * windowSize);
			Transform.copyShortSampleToFloatDomain(origInput, dftWindow, i * windowSize, n, windowSize, 0);

			// Apply Amplitude windowing function
			Transform.applyHanningWindow(dftWindow.real);
			
			// Get frequency domain
			Transform.fastFourierTransform(dftWindow, 0, windowSize, false);
			
			// Do pitch shift
			Transform.pitchShift(dftWindow, (2 * i + 1) * bendShift);
			
			// Get time domain back
			Transform.fastFourierTransform(dftWindow, 0, windowSize, true);
			
			// Re-add time section into new full time domain
			combinedDft.addAll(dftWindow.real);
		}
		
		/* Extract and scale the first odd positioned window */
		n = windowSize / 2;
		Transform.copyShortSampleToFloatDomain(origInput, dftWindow, 0, n, windowSize, windowSize / 2);
		
		// Apply Amplitude windowing function
		Transform.applyHanningWindow(dftWindow.real);

		// Get frequency domain
		Transform.fastFourierTransform(dftWindow, 0, windowSize, false);
		
		// Do pitch shift
		Transform.pitchShift(dftWindow, -1 * bendShift);
		
		// Get time domain back
		Transform.fastFourierTransform(dftWindow, 0, windowSize, true);

		// Re-add time section into new full time domain
		mergeFloatArrayOntoFloatArray(combinedDft, dftWindow.real, 0, n, windowSize / 2);
		
		stages = (int) Math.ceil((double)(origInput.sample.size - windowSize / 2)/ (double)windowSize);
		
		/* Extract and scale the rest of the odd positioned windows */
		for (int i = 0; i < stages; i++) {
			// Extract short sections of time domain
			n = Math.min(windowSize, origInput.sample.size - windowSize / 2 - i * windowSize);
			Transform.copyShortSampleToFloatDomain(origInput, dftWindow, i * windowSize + windowSize / 2, n, windowSize, 0);
	
			// Apply Amplitude windowing function
			Transform.applyHanningWindow(dftWindow.real);
			
			// Get frequency domain
			Transform.fastFourierTransform(dftWindow, 0, windowSize, false);
			
			// Do pitch shift
			Transform.pitchShift(dftWindow, 2 * i * bendShift);
			
			// Get time domain back
			Transform.fastFourierTransform(dftWindow, 0, windowSize, true);

			// Re-add time section into new full time domain
			mergeFloatArrayOntoFloatArray(combinedDft, dftWindow.real, i * windowSize + windowSize / 2, n, 0);
		}
		
		float scale = Short.MAX_VALUE;
		// Find the latest scale once all the samples have been merged
		
		for (int i = 0; i < combinedDft.size; i++) {
			if (scale < combinedDft.get(i)) scale = combinedDft.get(i);
		}
		scale *= 1.05; // Attempt to prevent rounding errors
		
		// convert float array back to short
		Transform.copyFloatDomainToShortSample(combinedDft, sample, scale, 0, Math.min(combinedDft.size, sample.sample.size), 0);

	}
	
	public static void mergeFloatArrayOntoFloatArray(FloatArray dst, FloatArray src, int dstOffset, int dstLen, int srcPrefix) {
		Gdx.app.log("mgFloats", "dstOffset = " + dstOffset + ", dstLen = " + dstLen+ ", srcPrefix = " + srcPrefix);
		
		dst.ensureCapacity((dstOffset + dstLen) - dst.size);
		
		for (int i = 0; i < dstLen; i++) {
			if (i + dstOffset >= dst.size) dst.add(0);
			float newValue = dst.get(i + dstOffset) + src.get(i + srcPrefix);
			dst.set(i + dstOffset, newValue);
		}
	}

}
