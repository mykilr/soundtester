package com.mykilr.effects;

import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.Array;

public class EffectFactory { 
	
	private Array<Class<? extends Effect>> effectList;
	private String[] names;
	private Skin skin;

	/**
	 * Get a factory that generate effects that have panels using the given skin
	 * @param skin
	 */
	public EffectFactory(final Skin skin) {
		this.skin = skin;
		effectList = new Array<Class<? extends Effect>>();
		
		// The added effects must line up with their names in the names list below
		effectList.add(AmplitudeEnvelope.class);
		effectList.add(GainModifier.class);
		effectList.add(PitchShift.class);
		effectList.add(Reverse.class);
		effectList.add(PitchBend.class);
		effectList.add(Repeat.class);
		effectList.add(Crop.class);
		effectList.add(Equaliser.class);
		
		// The added effect names must line up with the effects in the list above
		names = new String[] {
				"ADSR Envelope"
				,"Gain Modifier"
				,"Pitch Adjust"
				,"Reverse"
				,"PitchBend"
				,"Repeat"
				,"Crop"
				,"Equalizer"
		};
	}
	
	/**
	 * Get the list of available effect names from this factory
	 * @return
	 */
	public String[] getEffectNameList() {
		return names;
	}
	
	/**
	 * Get an effect using the index of it's name from the list returned by
	 * getEffectNameList()
	 * @param effectIndex
	 * @return new Effect object
	 */
	public  Effect getNewEffect(int effectIndex) {
		Effect retEffect = null;
		if (effectIndex < 0 || effectIndex > effectList.size) {
			return null;
		}
		/* Try to get an instance of the appropriate effect */
		try {
			retEffect = effectList.get(effectIndex).getConstructor(Skin.class).newInstance(skin);
		} catch (ReflectiveOperationException roe) {
			roe.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		}
		return retEffect;
	}
	
}
