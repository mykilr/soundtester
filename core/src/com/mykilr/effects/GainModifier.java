package com.mykilr.effects;

import java.util.Random;

import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Slider;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.utils.FloatArray;
import com.badlogic.gdx.utils.ShortArray;
import com.mykilr.tools.PCMData;
import com.mykilr.ui.CrossUpdateListener;

public class GainModifier extends Effect {
	
	private static final float GAIN_DEFAULT = 2.0f;
	private static final float GAIN_STEP = 0.1f;
	private static final float MAX_GAIN = 10.0f;
	private static final float MIN_GAIN = 0.0f;
	
	/* Can I get away with this? I think not */
	private static String effectName = "Gain Modifier";
	private static Random rand = new Random(System.currentTimeMillis());

	private Slider editBar;
	private TextField editField;
	private Table table;
	private PCMData outputSample;
	
	public GainModifier(final Skin skin) {
		table = new Table(skin);
		editBar = new Slider(MIN_GAIN, MAX_GAIN, GAIN_STEP, false, skin);
		editBar.setValue(GAIN_DEFAULT);
		editField = new TextField(String.valueOf(editBar.getValue()), skin);
		outputSample = new PCMData();
		
		editBar.addListener(new CrossUpdateListener(editBar, editField));
		editField.setTextFieldListener(new CrossUpdateListener(editField, editBar));
		
		table.add(new Label("Amplitude Multiplier:  ", skin));
		table.add(editBar);
		table.add(editField);
		table.row();
	}
	
	@Override
	public Table getGui() {
		return table;
	}

	@Override
	public void random() {
		float range = (MAX_GAIN - MIN_GAIN) / GAIN_STEP;
		float newGain = rand.nextFloat() * range * GAIN_STEP + MIN_GAIN;
		editBar.setValue(newGain);
	}

	@Override
	public void setSample(PCMData sample) {
		outputSample.set(sample);
	}

	@Override
	public PCMData update() {
		applyGainMultiplier(outputSample, editBar.getValue());
		return outputSample;
	}

	@Override
	public PCMData update(PCMData stageInput) {
		setSample(stageInput);
		return update();
	}
	
	@Override
	public String toString() {
		return effectName;
	}

	@Override
	public boolean hasGui() {
		return (table != null);
	}

	public static void applyGainMultiplier(PCMData data, float gainMultiplier) {
		ShortArray sample = data.sample;
		applyGainMultiplier(sample, gainMultiplier);
	}

	public static void applyGainMultiplier(ShortArray sample, float gainMultiplier) {
		for (int i=0; i < sample.size; i++) {
			// As long as the gain isn't too big, the new amp should be in Integer range
			int newAmplitude = (int) (sample.get(i) * gainMultiplier);
			// Try to avoid wrapping
			if (newAmplitude > Short.MAX_VALUE) newAmplitude = Short.MAX_VALUE;
			if (newAmplitude < Short.MIN_VALUE) newAmplitude = Short.MIN_VALUE;
			// Cast to short and set the data
			sample.set(i, (short) newAmplitude);
		}
	}

	public static void applyGainMultiplier(FloatArray sample, float gainMultiplier) {
		for (int i=0; i < sample.size; i++) {
			// As long as the gain isn't too big, the new amp should be in Integer range
			float newAmplitude = sample.get(i) * gainMultiplier;
			// Cast to short and set the data
			sample.set(i, newAmplitude);
		}
	}
}
