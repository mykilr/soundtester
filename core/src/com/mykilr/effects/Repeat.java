package com.mykilr.effects;

import java.util.Random;

import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Slider;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.FloatArray;
import com.mykilr.tools.PCMData;
import com.mykilr.tools.Transform;
import com.mykilr.ui.CrossUpdateListener;

public class Repeat extends Effect {

	private static final String effectName = "Repeat";

	private static final float TIME_MIN = 0.01f;
	private static final float TIME_MAX = 1.0f;
	private static final float TIME_STEP = 0.01f;
	private static final float TIME_DEFAULT = 0.25f;
	private static final float SCALE_MIN = 0.1f;
	private static final float SCALE_MAX = 1.50f;
	private static final float SCALE_STEP = 0.05f;
	private static final float SCALE_DEFAULT = 0.5f;
	
	private Random rand;
	private PCMData outputSample;
	
	private Table table;
	private Slider repeatTimeSlider;
	private TextField repeatTimeField;
	private Slider repeatScaleSlider;
	private TextField repeatScaleField;
	
	public Repeat(Skin skin) {
		outputSample = new PCMData();
		rand = new Random(System.currentTimeMillis());
		
		table = new Table(skin);
		repeatTimeSlider  = new Slider(TIME_MIN, TIME_MAX, TIME_STEP, false, skin);    
		repeatTimeField   = new TextField("", skin);
		repeatScaleSlider = new Slider(SCALE_MIN, SCALE_MAX, SCALE_STEP, false, skin);
		repeatScaleField  = new TextField("", skin);

		repeatTimeSlider.addListener(new CrossUpdateListener(repeatTimeSlider, repeatTimeField));
		repeatTimeField.setTextFieldListener(new CrossUpdateListener(repeatTimeField, repeatTimeSlider));

		repeatScaleSlider.addListener(new CrossUpdateListener(repeatScaleSlider, repeatScaleField));
		repeatScaleField.setTextFieldListener(new CrossUpdateListener(repeatScaleField, repeatScaleSlider));
		
		repeatTimeSlider.setValue(TIME_DEFAULT);
		repeatScaleSlider.setValue(SCALE_DEFAULT);
		
		table.add(new Label("Repeat Time (secs): ", skin)).align(Align.right);
		table.add(repeatTimeSlider);
		table.add(repeatTimeField);
		table.row();
		table.add(new Label("Repeat Scale: ", skin));
		table.add(repeatScaleSlider);
		table.add(repeatScaleField);
		table.row();
		table.add().height(160);
	}
	
	@Override
	public Table getGui() {
		return table;
	}

	@Override
	public void random() {
		float timeRange = (TIME_MAX - TIME_MIN) / TIME_STEP;
		float newTime = rand.nextFloat() * timeRange * TIME_STEP + TIME_MIN;
		repeatTimeSlider.setValue(newTime);
		float scaleRange = (SCALE_MAX - SCALE_MIN) / SCALE_STEP;
		float newScale = rand.nextFloat() * scaleRange * SCALE_STEP + SCALE_MIN;
		repeatScaleSlider.setValue(newScale);
	}

	@Override
	public void setSample(PCMData sample) {
		outputSample.set(sample);
	}

	@Override
	public PCMData update() {
		int sampleDelay = (int) (outputSample.sampleRate *  repeatTimeSlider.getValue());
		float repeatScale = repeatScaleSlider.getValue();
		sampleAddRepeat(outputSample, sampleDelay, repeatScale);
		return outputSample;
	}

	@Override
	public PCMData update(PCMData stageInput) {
		setSample(stageInput);
		return update();
	}

	@Override
	public boolean hasGui() {
		return table != null;
	}
	
	@Override
	public String toString() {
		return effectName;
	}
	
	private static void sampleAddRepeat(PCMData sample, int sampleDelay, float repeatScale){
		FloatArray echoArray = new FloatArray();
		
		// Annoying copy from short to float
		for (int i = 0; i < sample.sample.size; i++) {
			echoArray.add(sample.sample.get(i));
		}
		
		float convertScale = createEcho(echoArray, sampleDelay, repeatScale);
		
		Transform.copyFloatDomainToShortSample(echoArray, sample, convertScale, 0, echoArray.size, 0);
	}
	
	/**
	 * Copy sample data left or right
	 * @param data
	 * @param offset
	 */
	private static float createEcho(FloatArray data, int offset, float scale) {
		float maxAbsValue = Short.MAX_VALUE;
		// Increase sample size by echo length
		FloatArray origData = new FloatArray(data);
		int origDataSize = data.size;
		
		data.ensureCapacity(offset);
		for (int i = 0; i < offset; i++) {
			data.add(0.0f);
		}
		
		// If the offset is negative (pre-echo?) 
		if (offset < 0) {
			// move the sample forward
			for (int i = origDataSize + offset - 1; i >= offset; i--) {
				float value = data.get(i + offset);
				maxAbsValue = maxAbs(maxAbsValue, value);
				data.set(i, value);
			}
			for (int i = offset - 1; i >= 0; i--) {
				data.set(i, 0.0f);
			}
			// and copy towards the start of the array
			for (int i = origDataSize - 1; i >= 0; i--) {
				float value = data.get(i) + origData.get(i) * scale;
				maxAbsValue = maxAbs(maxAbsValue, value);
				data.set(i, value);
			}
		// otherwise, if the offset is positive
		} else if (offset > 0) {
			// Update maxAbsValue from the existing samples
			for (int i = 0; i < offset; i++) {
				float value = data.get(i);
				maxAbsValue = maxAbs(maxAbsValue, value);
			}
			// copy towards the end of the array
			for (int i = offset; i < origDataSize + offset; i++) {
				float value = data.get(i) + origData.get((i - offset)) * scale;
				maxAbsValue = maxAbs(maxAbsValue, value);
				data.set(i, value);
			}
		}
		return maxAbsValue;
	}
	
	private static float maxAbs(float a, float b) {
		return Math.max(Math.abs(a), Math.abs(b));
	}


}
