package com.mykilr.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.mykilr.SoundTestGame;
import com.mykilr.tools.PCMData;
import com.mykilr.tools.PCMDomain;
import com.mykilr.tools.Transform;
import com.mykilr.ui.ZoomableSample;

public class ShowDFTScreen implements Screen {

	private static final String PHASE = "Phase";
	private static final String MAGNATUDE = "Magnatude";
	private static final String IMAGINARY = "Imaginary";
	private static final String REAL = "Real";
	public static final double PI = Math.PI;
	private Stage stage;
	private Color clearColor;
	private PCMData sample;
	private PCMDomain dft;
	
	private Table table;
	private Label progressField;
	private TextButton okayButton;
	// Rectangular notation
	private ZoomableSample realPart;
	private ZoomableSample imaginaryPart;
	private Label realLabel;
	private Label imaginaryLabel;
	// Polar notation
	private ZoomableSample magnatudePart;
	private ZoomableSample phasePart;
	private Label phaseLabel;
	private Label magnatudeLabel;
	
	// Dft draw threading
	private boolean stop = false;
	private boolean applyProgressUpdate;
	private boolean applyDftUpdate;
	
	public ShowDFTScreen(final Skin skin, final SoundTestGame soundTestGame) {
		// Setup data
		sample = new PCMData();
		dft = new PCMDomain();
		
		// Setup GUI
		clearColor = Color.LIGHT_GRAY;
		stage = new Stage();
		
		table = new Table(skin);
		progressField = new Label("Progress", skin);
		okayButton = new TextButton("Okay", skin);
		realPart = new ZoomableSample(sample.sample, skin);
		imaginaryPart = new ZoomableSample(sample.sample, skin);
		magnatudePart = new ZoomableSample(sample.sample, skin);
		phasePart = new ZoomableSample(sample.sample, skin);
		realLabel = new Label(REAL, skin);
		imaginaryLabel = new Label(IMAGINARY, skin);
		magnatudeLabel = new Label(MAGNATUDE, skin);
		phaseLabel = new Label(PHASE, skin);
		
		// Define Behaviours
		okayButton.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				if (!okayButton.isDisabled()) {
					stop = true;
					soundTestGame.dftShown();
				}
			}
		});
		
		// Create layout
		table.add(realPart);
		table.add(realLabel).align(Align.left);
		table.row();
		table.add(imaginaryPart);
		table.add(imaginaryLabel).align(Align.left);
		table.row();
		table.add(magnatudePart);
		table.add(magnatudeLabel).align(Align.left);
		table.row();
		table.add(phasePart);
		table.add(phaseLabel).align(Align.left);
		table.row();
		
		table.add(progressField);
		table.add(okayButton).width(160);
		table.row();  
		
		table.setFillParent(true);
		stage.addActor(table);
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(clearColor.r, clearColor.g, clearColor.b, clearColor.a);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		// Draw GUI
		
		if (applyProgressUpdate) {
//			progressField.setText(String.format("Progress: %d/%d ", progressStage, progressStages) + tdp(progressUpdate) + "%");
			applyProgressUpdate = false;
		}
		
		if(applyDftUpdate) {
			
			float minIm = Float.MAX_VALUE;
			float maxIm = Float.MIN_VALUE;
			float minRe = Float.MAX_VALUE;
			float maxRe = Float.MIN_VALUE;
			float minMa = Float.MAX_VALUE;
			float maxMa = Float.MIN_VALUE;
			float minPh = Float.MAX_VALUE;
			float maxPh = Float.MIN_VALUE;
			
			int maxMaInd = -1;
			
			// Only worry about max in the first half of the frequency domain
			for (int i=0; i < dft.real.size / 2; i++) {
				minRe = (minRe <= dft.real.get(i)) ? minRe : dft.real.get(i);
				maxRe = (maxRe >= dft.real.get(i)) ? maxRe : dft.real.get(i);
				minIm = (minIm <= dft.imaginary.get(i)) ? minIm : dft.imaginary.get(i);
				maxIm = (maxIm >= dft.imaginary.get(i)) ? maxIm : dft.imaginary.get(i);
				minPh = (minPh <= dft.phase.get(i)) ? minPh : dft.phase.get(i);
				maxPh = (maxPh >= dft.phase.get(i)) ? maxPh : dft.phase.get(i);
				minMa = (minMa <= dft.magnitude.get(i)) ? minMa : dft.magnitude.get(i);
				if (maxMa < dft.magnitude.get(i)) {
					maxMaInd = i;
					maxMa = dft.magnitude.get(i);
				}
			}
			
			float scaleRe = Math.max(Math.abs(minRe), Math.abs(maxRe)) * 2;
			float scaleIm = Math.max(Math.abs(minIm), Math.abs(maxIm)) * 2;
			float scaleMa = Math.max(Math.abs(minMa), Math.abs(maxMa)) * 2;
			float scalePh = Math.max(Math.abs(minPh), Math.abs(maxPh)) * 2;
			
			if (scaleRe == 0.0f) scaleRe = 1;
			if (scaleIm == 0.0f) scaleIm = 1;
			if (scaleMa == 0.0f) scaleMa = 1;
			if (scalePh == 0.0f) scaleMa = 1;
			
			realLabel.setText(tdp(scaleRe) + "\n" + REAL + "\n" + tdp(-scaleRe));
			imaginaryLabel.setText(tdp(scaleIm) + "\n" + IMAGINARY + "\n" + tdp(-scaleIm));
			magnatudeLabel.setText(tdp(scaleMa) + "\n" + MAGNATUDE + "\n" + tdp(-scaleMa));
			phaseLabel.setText(tdp(scalePh) + "\n" + PHASE + "\n" + tdp(-scalePh));
			
			float guessedFreq = (sample.sampleRate * (maxMaInd) / dft.magnitude.size);
			
			progressField.setText("Sample Len: " + (dft.real.size - dft.padding) + ", Freq Domain Len: " + dft.real.size +
					", Approx Freq: " + tdp(guessedFreq) + " (index: " + maxMaInd + ")");
			
			realPart.setSampleData(dft.real, 1f / scaleRe);
			imaginaryPart.setSampleData(dft.imaginary, 1f / scaleIm);
			magnatudePart.setSampleData(dft.magnitude, 1f / scaleMa);
			phasePart.setSampleData(dft.phase, 1f / scalePh);
			applyDftUpdate = false;
		}
		
		stage.act(delta);
		stage.draw();
	}
	
	/**
	 * Two Decimal Places
	 * @param number
	 * @return
	 */
	private static String tdp(float number) {
		return String.format("%03.2f", number);
	}

	@Override
	public void resize(int width, int height) {
		stage.getViewport().update(width, height, true);
	}

	@Override
	public void show() {
		Gdx.input.setInputProcessor(stage);
		
		stop = false;
		Thread thread = new Thread(runner);
		thread.start();
	}
	
	private Runnable runner = new Runnable() {

		@Override
		public void run() {
//			discreteFourierTransform(sample, 0, sample.sample.size, dft);
			fastFourierTransform(sample, dft);
			
			if (!stop) applyDftUpdate = true;
		}
	};
		
	@Override
	public void hide() {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void dispose() {
		stage.dispose();
	}

	public void setSelectedPCMData(PCMData sample) {
		Gdx.app.log("DFT","Updating Sample");
		this.sample.set(sample);
	}
	
	private void fastFourierTransform(PCMData realSample, PCMDomain dft) {
		// Just started

		// Checks
		if (realSample == null || dft == null) throw new IllegalArgumentException();
		
		int snipSize = Transform.copyShortSampleToFloatDomain(realSample, dft);

		applyProgressUpdate = true;
		
		int offset = 0;
		Transform.fastFourierTransform(dft, offset, snipSize, false);

		dft.updatePolarFromRectangular();

		applyProgressUpdate = true;

		Gdx.app.log("DFT","Transform Done. Interupted: " + stop);
	}
	
	
}
