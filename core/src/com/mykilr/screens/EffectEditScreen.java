package com.mykilr.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.mykilr.SoundTestGame;
import com.mykilr.effects.Effect;
import com.mykilr.tools.PCMData;

public class EffectEditScreen implements Screen{
	
	private Stage stage;
	private Effect effect;
	private Color clearColor;
	private Table rootTable;
	private Table commonControls;
	private TextButton randomButton;
	// private TextButton playButton;
	private TextButton keepButton;
	// private TextButton cancelButton;
	private PCMData sample;
	// private NoisePlayer player;

	public EffectEditScreen(final Skin skin, final SoundTestGame soundTestGame) {
		clearColor = Color.LIGHT_GRAY;
		stage = new Stage(new FitViewport(1280, 736));
		rootTable = new Table(skin);
		commonControls = new Table(skin);
		
		sample = new PCMData();
//		player = new NoisePlayer(sample.sample, sample.sampleRate, sample.channels == 1);
		
		randomButton = new TextButton("Random", skin);
		randomButton.addListener(randomListener);
		// playButton = player.getPlayStopButton("Play", "Stop", skin);
		keepButton = new TextButton("Okay", skin);
		keepButton.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				soundTestGame.effectConfigured(true, effect, sample);
			}
		});
//		cancelButton = new TextButton("Cancel", skin);
//		cancelButton.addListener(new ClickListener() {
//			@Override
//			public void clicked(InputEvent event, float x, float y) {
//				soundTestGame.effectConfigured(false, effect, sample);
//			};
//		});
		
		commonControls.add(randomButton).width(160);
		// commonControls.add(playButton).width(160);
		commonControls.add(keepButton).width(160);
		// commonControls.add(cancelButton).width(160);
		commonControls.row();
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(clearColor.r, clearColor.g, clearColor.b,
				clearColor.a);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		// Draw GUI
		stage.act(delta);
		stage.draw();
	}

	@Override
	public void resize(int width, int height) {
		stage.getViewport().update(width, height, true);
	}

	@Override
	public void show() {
		Gdx.input.setInputProcessor(stage);
		if (effect != null) {
			stage.clear();
			rootTable.clear();
			Table gui = effect.getGui();
			rootTable.add(gui);
			rootTable.row();
			rootTable.add(commonControls);
			rootTable.setFillParent(true);
			stage.addActor(rootTable);
		}
		
	}

	@Override
	public void hide() {
		
	}

	@Override
	public void pause() {
		
	}

	@Override
	public void resume() {
		
	}

	@Override
	public void dispose() {
		stage.dispose();
	}
	
	public void setSample(PCMData data) {
		this.sample.set(data);
		// player.setSampleData(sample);
	}

	public void setEffect(Effect effect) {
		this.effect = effect;
//		if (effect != null) {
//			effect.setSample(sample);
//		}
	}
	
	private ClickListener randomListener = new ClickListener() {
		@Override
		public void clicked(InputEvent event, float x, float y) {
			if (effect != null) {
				effect.random();
			}
		};
	};

}
