package com.mykilr.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.mykilr.SoundTestGame;
import com.mykilr.tools.NoisePlayer;
import com.mykilr.tools.NoiseRecorder;
import com.mykilr.tools.PCMData;

public class RecordingScreen implements Screen {

	// Gui
	private Stage stage;
	private Table table;
	private Label timeLabel;
	// Playback controls
	private TextButton recordButton;
	private TextButton playButton;
	private TextButton keepButton;
	// private TextButton cancelButton;
	// Sample Creation
	private PCMData sample;
	private NoisePlayer player;
	private NoiseRecorder recorder;
	
	public RecordingScreen(Skin skin, final SoundTestGame soundTestGame) {
		
		// Setup Sample Storage
		sample = new PCMData();
		recorder = new NoiseRecorder(sample.sample, sample.sampleRate, sample.channels == 1);
		player = new NoisePlayer(sample);
		
		// setup GUI
		stage = new Stage(new FitViewport(1280, 736));
		Gdx.input.setInputProcessor(stage);
		
		// Create GUI Components
		table = new Table();

		timeLabel = new Label("Sample Time: 0.00 Seconds", skin);

		recordButton = recorder.getRecordStopButton("Record", " Stop ", skin);
		playButton = player.getPlayStopButton(" Play ", " Stop ", skin);
		
		keepButton = new TextButton(" Okay ", skin);
		// cancelButton = new TextButton("Cancel", skin);
		
		// Setup mechanics
		keepButton.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				if (!keepButton.isDisabled()) {
					player.stop();
					recorder.stop();
					soundTestGame.toneRecorded(true, sample);
				}
			};
		});
//		cancelButton.addListener(new ClickListener() {
//			@Override
//			public void clicked(InputEvent event, float x, float y) {
//				soundTestGame.toneRecorded(false, sample);
//			};
//		});
		

		table.add(timeLabel).align(Align.center).colspan(3);
		table.row();
		
		table.add(recordButton);
		table.add(playButton);
		table.add(keepButton);
//		table.add(cancelButton);
		
		table.setFillParent(true);
		table.align(Align.center);
		stage.addActor(table);
	}

	@Override
	public void render(float delta) {
		// Update the time length field
		float sampleLenSec = (float)sample.sample.size / (float)sample.sampleRate;
		timeLabel.setText(String.format("Sample Time: %2.2f seconds", sampleLenSec));
		
		// Setup drawing surface
		Color clearColor = Color.LIGHT_GRAY;
		Gdx.gl.glClearColor(clearColor.r, clearColor.g, clearColor.b,
				clearColor.a);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		stage.act(delta);
		stage.draw();
	}

	@Override
	public void resize(int width, int height) {
		stage.getViewport().update(width, height);
	}
	
	@Override
	public void show() {
		Gdx.input.setInputProcessor(stage);
	}

	@Override
	public void hide() {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void dispose() {
		stage.dispose();
		player.dispose();
		recorder.dispose();

	}

}
