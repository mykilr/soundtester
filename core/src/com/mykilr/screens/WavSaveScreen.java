package com.mykilr.screens;

import java.io.File;
import java.io.FileFilter;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Dialog;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.List;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.mykilr.SoundTestGame;
import com.mykilr.tools.PCMData;
import com.mykilr.tools.PCMUtils;

public class WavSaveScreen implements Screen {

	private static final String FILE_NAME        = "FileName: ";
	private static final String NEW_SAMPLE_WAV   = "new_sample.wav";
	private static final String CANCEL           = "Cancel";
	private static final String OPEN             = "Open";
	private static final String SAVE             = "Save";
	private static final String OVERWRITE        = "Overwrite";
	private static final String SAVE_DIALOG      = "Save file to: ";
	private static final String OVERWRITE_DIALOG = "Overwrite file at: ";
	
	private Color clearColor;
	private Color fadeColor;
	
	private Stage stage;
	private Table table;
	
	private PCMData data;
	
	private FileHandle currentPath;
	private FileHandle[] currentChildren;
	private List<String> folderContents;
	private ScrollPane listPane;
	private Label currentPathLabel;
	private TextField fileNameField;
	
	private TextButton openButton;
	private TextButton saveButton;
	private Dialog saveDialog;
	private TextButton cancelButton;
	private Label saveDialogLabel;
	private TextButton saveDialogSaveButton;
	
	public WavSaveScreen(Skin skin, final SoundTestGame soundTestGame) {
		// Screen draw setup
		clearColor = Color.LIGHT_GRAY;
		fadeColor = new Color(Color.LIGHT_GRAY);
		fadeColor.a = 0.85f;
		
		// GUI
		stage = new Stage();
		table = new Table();
		
		currentPathLabel = new Label("", skin);
		folderContents = new List<String>(skin);
		listPane = new ScrollPane(folderContents);
		
		currentPath = Gdx.files.external("");
		changeDirectory(currentPath);
		
		openButton = new TextButton(OPEN, skin);
		saveButton = new TextButton(SAVE, skin);
		cancelButton = new TextButton(CANCEL, skin);
		
		saveDialogLabel = new Label("Save file as: ", skin);
		saveDialogSaveButton = new TextButton(SAVE, skin);
		saveDialog = setupSaveDialogWindow(skin);
		
		Label fileNameLabel = new Label(FILE_NAME, skin);
		fileNameField = new TextField(NEW_SAMPLE_WAV, skin);
		
		// Add behaviour mechanics
		openButton.setDisabled(true);
		openButton.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				openDirectory();
			}
		});
		folderContents.addListener(new ClickListener() {
			@Override
			public void touchUp(InputEvent event, float x, float y,
					int pointer, int button) {
				super.touchUp(event, x, y, pointer, button);
				fileSelected(folderContents.getSelectedIndex());
			}
		});
		fileNameField.setTextFieldListener(new TextField.TextFieldListener() {
			@Override
			public void keyTyped(TextField textField, char c) {
				fileNameEdited();
			}
		});
		saveButton.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				saveDialogLabel.setText(getSaveDialogText());
				saveDialog.show(stage);
			}
		});
		cancelButton.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				soundTestGame.fileSaved();
			}
		});
		saveDialogSaveButton.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				PCMUtils.savePCMToWav(data, currentPath.child(fileNameField.getText()));
				saveDialog.hide();
				soundTestGame.fileSaved();
			}
		});
		
		// Layout
		table.add(fileNameLabel).colspan(1);
		table.add(fileNameField).width(500).colspan(2);
		table.add();
		table.add();
		table.row();
		
		table.add();
		table.add(currentPathLabel).colspan(3).align(Align.left);
		table.row();
		
		table.add();
		table.add(listPane).colspan(2).width(500).height(360);
		Table buttons = new Table();
		buttons.add(openButton).width(160);
		buttons.row();
		buttons.add(saveButton).width(160);
		buttons.row();
		buttons.add(cancelButton).width(160);
		table.add(buttons).colspan(1);
		table.row();
		
		table.setFillParent(true);
		stage.addActor(table);
	}

	protected CharSequence getSaveDialogText() {
		FileHandle newFile = currentPath.child(fileNameField.getText());
		if (newFile.exists()) {
			return OVERWRITE_DIALOG + newFile.path() + "?";
		} else {
			return SAVE_DIALOG + newFile.path() + "?";
		}
	}

	private Dialog setupSaveDialogWindow(Skin skin) {
		TextButton saveDialogCancelButton = new TextButton(CANCEL, skin);
		final Dialog dialog = new Dialog("", skin);
		
		// Create the background fade
		Pixmap pixmap = new Pixmap(1, 1, Format.RGBA8888);
		pixmap.setColor(fadeColor);
		pixmap.drawPixel(0, 0);
		dialog.getStyle().stageBackground = new SpriteDrawable(
				new Sprite(new Texture(pixmap)));
		dialog.getStyle().background = new SpriteDrawable(
				new Sprite(new Texture(pixmap)));

		// Add behaviour mechanisms
		saveDialogCancelButton.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				dialog.hide();
			}
		});
		
		// Dialog Layout
		dialog.row();
		dialog.add(saveDialogLabel).colspan(2);
		dialog.row();
		dialog.add(saveDialogSaveButton).width(160);
		dialog.add(saveDialogCancelButton).width(160);
		return dialog;
	}

	protected void fileNameEdited() {
		if (currentPath.child(fileNameField.getText()).exists()) {
			saveButton.setText(OVERWRITE);
			saveDialogSaveButton.setText(OVERWRITE);
		} else {
			saveButton.setText(SAVE);
			saveDialogSaveButton.setText(SAVE);
		}
	}

	private void fileSelected(int selectedIndex) {
		FileHandle fileHandle = currentChildren[selectedIndex];
		if (wavFilter.accept(fileHandle.file())) {
			if (fileHandle.isDirectory()) {
				openButton.setDisabled(false);
			} else {
				fileNameField.setText(fileHandle.name());
				fileNameEdited();
				openButton.setDisabled(true);
			}
		}
	}

	private void openDirectory() {
		FileHandle child = currentChildren[folderContents.getSelectedIndex()];
		if (child.isDirectory()) {
			changeDirectory(child);
			fileNameEdited();
		}
	}
	
	private void changeDirectory(FileHandle newDir) {
		if (newDir.isDirectory()) {
			currentPath = newDir;
			currentPathLabel.setText("/" + newDir.path());
			currentChildren = getWavListWithParent(newDir);
			folderContents.setItems(getStringArrayFromFileHandles(currentChildren));
		}
		
	}
	
	private FileHandle[] getWavListWithParent(FileHandle dir) {
	
		FileHandle[] tempList = dir.list(wavFilter);
		if (dir.parent().equals(dir)) {
			return tempList;
		}
		FileHandle[] retList = new FileHandle[tempList.length + 1];
		retList[0] = dir.parent();
		for (int i=0; i<tempList.length; i++) {
			retList[i+1] = tempList[i];
		}
		return retList;
	}
	
	FileFilter wavFilter = new FileFilter(){
		@Override
		public boolean accept(File pathname) {
			if (pathname.isHidden()) return false;
			if (pathname.getName().startsWith(".")) return false;
			if (pathname.isDirectory()) return true;
			if (pathname.getName().endsWith(".wav")) return true;
			return false;
		}
		
	};
		
	private String[] getStringArrayFromFileHandles(FileHandle[] handles) {
		String[] ret = new String[handles.length];
		for (int i=0; i<handles.length; i++) {
			String name = handles[i].name();
			ret[i] = (name.length() > 30 ? name.substring(0,  27) + "..." : name) +
				(handles[i].isDirectory() ? "/" : "");
		}
		// If the first item is the parent
		if (handles[0].equals(currentPath.parent())) {
			ret[0] = "../";
		}
		return ret;
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(clearColor.r, clearColor.g, clearColor.b,
				clearColor.a);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		// Draw GUI
		stage.act(delta);
		stage.draw();
	}

	@Override
	public void resize(int width, int height) {
		stage.getViewport().update(width, height, true);
	}

	@Override
	public void show() {
		fileNameField.setText(NEW_SAMPLE_WAV);
		changeDirectory(currentPath);
		fileNameEdited();
		Gdx.input.setInputProcessor(stage);
	}

	@Override
	public void hide() {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void dispose() {
		stage.dispose();
	}

	public void setData(PCMData data) {
		this.data = data;
	}

}
