package com.mykilr.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.List;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.SelectBox;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Array;
import com.mykilr.SoundTestGame;
import com.mykilr.effects.Effect;
import com.mykilr.effects.EffectFactory;
import com.mykilr.tools.NoisePlayer;
import com.mykilr.tools.PCMData;
import com.mykilr.ui.ZoomableSample;

public class SountTestScreen implements Screen {

	private ZoomableSample zoomGraph;
	private NoisePlayer player;
	private EffectFactory effectFactory;
	
	// Main layout
	private Stage stage;
	private Table mainTable;
	// Input Panel
	private Table inputTable;
	private TextButton generateButton;
	// private CheckBox generateCheck;
	private TextButton recordButton;
	// private CheckBox recordCheck;
	private TextButton loadButton;
	// private CheckBox loadCheck;
	// Processes Panel
	// Add more effects, maybe allow changing order
	private Table effectsTable;
//	private TextButton amplitudeButton;
//	private CheckBox amplitudeCheck;
	
	private List<Effect> activeEffects;
	private ScrollPane listPane;
	private SelectBox<String> chooseEffect;
	private TextButton addEffectButton;
	private TextButton editEffectButton;
	private TextButton deleteEffectButton;
	
	// Output Panel
	private Table outputTable;
	private TextButton playButton;
	private TextButton graphButton;
	private TextButton saveButton;
	// Data stages
	private PCMData inputData;
	private PCMData outputData;
	
	public SountTestScreen(Skin skin, final SoundTestGame soundTestGame) {
		
		inputData = new PCMData();
		outputData = new PCMData();
		zoomGraph = new ZoomableSample(outputData.sample, skin);
		player = new NoisePlayer(outputData);
		effectFactory = new EffectFactory(skin);
		
		// Main table object creation
		stage = new Stage();
		mainTable = new Table(skin);
		mainTable.setFillParent(true);
		// Input Panel
		inputTable = new Table(skin);
		generateButton = new TextButton(" Tone ", skin);
		recordButton = new TextButton("Record", skin);
		loadButton = new TextButton(" Load ", skin);
		// Processes Panel
		// Add more effects, maybe allow changing order
		effectsTable = new Table(skin);

		activeEffects= new List<Effect>(skin);
		activeEffects.setItems(new Array<Effect>());
		listPane = new ScrollPane(activeEffects);
		chooseEffect = new SelectBox<String>(skin);
		chooseEffect.setItems(effectFactory.getEffectNameList());
		
		addEffectButton = new TextButton("Add", skin); 
		editEffectButton = new TextButton("Edit", skin); 
		deleteEffectButton = new TextButton("Delete", skin);
		
		// Output Panel
		outputTable = new Table(skin);
		playButton = player.getPlayStopButton("Play",  "Stop", skin);
		saveButton = new TextButton("Save", skin);
		graphButton = new TextButton("FFT", skin);
		
		// Add behaviour mechanisms
		generateButton.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				if (!generateButton.isDisabled()) {
					soundTestGame.generateTone();
				}
			}
		});
		recordButton.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				if (!recordButton.isDisabled()) {
					soundTestGame.recordSample();
				}
			}
		});
		loadButton.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				if (!loadButton.isDisabled()) {
					soundTestGame.loadWavFile();
				}
			}
		});
		saveButton.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				if (!saveButton.isDisabled()) {
					soundTestGame.saveWavFile(outputData);
				}
			}
		});
		graphButton.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				if (!graphButton.isDisabled()) {
					soundTestGame.showDFT(outputData);
				}
			}
		});
		addEffectButton.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				if (!addEffectButton.isDisabled()) {
					Effect effect = effectFactory.getNewEffect(chooseEffect.getSelectedIndex());
					activeEffects.getItems().add(effect);
					activeEffects.setSelectedIndex(activeEffects.getItems().size - 1);
					updateEffects();
				}
			}
		});
		editEffectButton.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				if (!editEffectButton.isDisabled()) {
					int selectedIndex = activeEffects.getSelectedIndex();
					soundTestGame.configEffect(activeEffects.getItems().get(selectedIndex));
				}
			}
		});
		deleteEffectButton.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				if (!deleteEffectButton.isDisabled()) {
					activeEffects.getItems().removeIndex(activeEffects.getSelectedIndex());
					updateEffects();
				}
			}
		});
		activeEffects.addListener(new ClickListener() {
			@Override
			public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
				super.touchUp(event, x, y, pointer, button);
				updateEffects();
			}
		});
		
		// GUI Object layout
		// Input Panel
		inputTable.add("Input").colspan(2).align(Align.top);;
		inputTable.row();
//		inputTable.add(generateCheck);
		inputTable.add(generateButton).width(160);
		inputTable.row();
//		inputTable.add(recordCheck);
		inputTable.add(recordButton).width(160);
		inputTable.row();
//		inputTable.add(loadCheck);
		inputTable.add(loadButton).width(160);
		inputTable.row();
		// Processes Panel
		effectsTable.add("Effects").align(Align.top);
		effectsTable.row();
//		effectsTable.add(amplitudeCheck);
//		effectsTable.add(amplitudeButton);
		
		effectsTable.add(listPane).width(250).height(300);
		Table effectControls = new Table();
		effectControls.add(chooseEffect).width(220);
		effectControls.row();
		effectControls.add(addEffectButton).width(160);
		effectControls.row();
		effectControls.add(editEffectButton).width(160);
		effectControls.row();
		effectControls.add(deleteEffectButton).width(160);
		effectControls.row();
		effectsTable.add(effectControls);
		effectsTable.row();
		// Output Panel
		outputTable.add("Output").colspan(2).align(Align.top);
		outputTable.row();
		outputTable.add(playButton).width(160);
		outputTable.row();
		outputTable.add(saveButton).width(160);
		outputTable.row();
		outputTable.add(graphButton).width(160);
		outputTable.row();
		// Main panel
		mainTable.add(inputTable).align(Align.top);
		mainTable.add(effectsTable).align(Align.top);
		mainTable.add(outputTable).align(Align.top);
		mainTable.row();
		mainTable.add(zoomGraph).colspan(3);
		mainTable.row();
		
		updateEffects();
		stage.addActor(mainTable);
	}
	
	private void updateEffects() {
		boolean disable = activeEffects.getSelectedIndex() < 0;
		editEffectButton.setDisabled(disable || !activeEffects.getSelected().hasGui());
		deleteEffectButton.setDisabled(disable);
		
		// Filter sample up through the Effect stack
		// TODO: Maybe can only filter up the last few effects were needed
		PCMData stageInput = inputData;
		Array<Effect> items = activeEffects.getItems();
		for (int i = 0; i < items.size; i++) {
			stageInput = items.get(i).update(stageInput);
		}
		outputData.set(stageInput);
		zoomGraph.setSampleData(outputData.sample);
	}
	
	@Override
	public void render(float delta) {
		// Setup drawing surface
		Color clearColor = Color.LIGHT_GRAY;
		Gdx.gl.glClearColor(clearColor.r, clearColor.g, clearColor.b,
				clearColor.a);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		// Draw GUI
		stage.act(delta);
		stage.draw();
	}

	@Override
	public void resize(int width, int height) {
		stage.getViewport().update(width, height, true);
	}

	@Override
	public void show() {
		updateEffects();
		if (player == null) {
			player = new NoisePlayer(outputData);
		} else {
			player.setSampleData(outputData);
		}
		Gdx.input.setInputProcessor(stage);
	}

	@Override
	public void hide() {}

	@Override
	public void pause() {}

	@Override
	public void resume() {}

	@Override
	public void dispose() {
		stage.dispose();
	}

	public void setSelectedPCMData(final PCMData sample) {
		inputData = sample;
	}

}
