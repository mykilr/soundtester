package com.mykilr.screens;

import java.util.Random;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.SelectBox;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Slider;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.mykilr.SoundTestGame;
import com.mykilr.tools.NoisePlayer;
import com.mykilr.tools.Note;
import com.mykilr.tools.PCMData;
import com.mykilr.tools.WaveForm;
import com.mykilr.ui.CrossUpdateListener;

public class SampleGenerateScreen implements Screen {
	
	private static final float FREQUENCY_MIN = Note.C0.freq;
	private static final float FREQUENCY_MAX = Note.B9.freq;
	private static final float FREQUENCY_QUANTA = 0.01f;
	private static final float FREQUENCY_RANDOM_MIN = Note.C2.freq;
	private static final float FREQUENCY_RANDOM_MAX = Note.E9.freq;
	private static final float TIME_RANDOM_MIN = 0.1f;
	private static final float TIME_RANDOM_MAX = 0.9f;
	// Gui
	private Stage stage;
	private Table table;
	// Wave type
	private SelectBox<WaveForm> waveFormSelectBox;
	private TextField timeField;
	// Frequency Controls
	private Slider frequencySlider;
	private TextField frequencyField;
	private SelectBox<Note> frequencySelectBox;
	// Creation and Playback controls
	private TextButton randomButton;
	private TextButton playButton;
	private float testReclickTime;
	private TextButton keepButton;
	// Sample Creation
	private Random rand;
	private PCMData sample;
	private NoisePlayer player;
	
	public SampleGenerateScreen(Skin skin, final SoundTestGame soundTestGame) {
		
		// Setup Sample Storage
		rand = new Random(System.currentTimeMillis());
		sample = new PCMData();
		player = new NoisePlayer(sample);
		
		// setup GUI
		stage = new Stage(new FitViewport(1280, 736));
		Gdx.input.setInputProcessor(stage);
		
		// Create GUI Components
		table = new Table();
		
		Label waveFormSelectLabel = new Label("Wave Form:", skin);
		waveFormSelectBox = new SelectBox<WaveForm>(skin);
		waveFormSelectBox.setItems(WaveForm.WAVE_FORM_LIST);
		
		Label timeLabel = new Label("Sample Time:", skin);
		timeField = new TextField("0.1", skin);
		
		Label frequencyLabel = new Label("Frequency:", skin);
		frequencySlider = new Slider(FREQUENCY_MIN, FREQUENCY_MAX, FREQUENCY_QUANTA, false, skin);
		frequencyField = new TextField(String.valueOf(frequencySlider.getValue()), skin);
		frequencySelectBox = new SelectBox<Note>(skin);
		frequencySelectBox.setItems(Note.NOTE_LIST);
		frequencySlider.addListener(new CrossUpdateListener(frequencySlider, frequencyField));
		frequencySlider.addListener(new CrossUpdateListener(frequencySlider, frequencySelectBox));
		frequencyField.setTextFieldListener(new CrossUpdateListener(frequencyField, frequencySelectBox));
		frequencyField.setTextFieldListener(new CrossUpdateListener(frequencyField, frequencySlider));
		frequencySelectBox.addListener(new CrossUpdateListener(frequencySelectBox, frequencyField));
		frequencySelectBox.addListener(new CrossUpdateListener(frequencySelectBox, frequencySlider));
		frequencySlider.setValue(440.0f);
		
		randomButton = new TextButton("Random", skin);
		playButton = new TextButton("Play", skin);
//		playButton = player.getPlayStopButton("Test", "Stop", skin);
		keepButton = new TextButton("Okay", skin);
//		keepButton.setDisabled(true);
//		cancelButton = new TextButton("Cancel", skin);
		testReclickTime = 0.0f;
		
		// Setup mechanics
		randomButton.addListener(randomButtonListener);
		playButton.addListener(playButtonListener);
		keepButton.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				if (!keepButton.isDisabled()) {
					player.stop();
					playButton.setDisabled(false);
					testReclickTime = 0.0f;
					createSample();
					soundTestGame.toneGenerated(true, sample);
				}
			};
		});

		// Configure actions
		waveFormSelectBox.addListener(new InputListener() {});
		
		// Position GUI Components
		table.add(waveFormSelectLabel).align(Align.right);
		table.add(waveFormSelectBox);
		table.add(timeLabel).align(Align.right);
		table.add(timeField).align(Align.left);
		table.row();
		
		table.add(frequencyLabel).align(Align.right);
		table.add(frequencySlider);
		table.add(frequencyField);
		table.add(frequencySelectBox);
		table.row();
		
		table.add(randomButton).width(160);
		table.add(playButton).width(160);
		table.add();
		table.add(keepButton).width(160);
		
		table.setFillParent(true);
		table.align(Align.center);
		stage.addActor(table);
	}

	@Override
	public void render(float delta) {
		// Setup drawing surface
		Color clearColor = Color.LIGHT_GRAY;
		Gdx.gl.glClearColor(clearColor.r, clearColor.g, clearColor.b,
				clearColor.a);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		stage.act(delta);
		stage.draw();
		
		testReclickTime = testReclickTime > 0.0f ? testReclickTime - delta : 0.0f;
		if (testReclickTime <= 0.0f) {
			playButton.setDisabled(false);
			testReclickTime = 0.0f;
		}
	}

	@Override
	public void resize(int width, int height) {
		stage.getViewport().update(width, height);
	}
	
	@Override
	public void show() {
		Gdx.input.setInputProcessor(stage);
	}

	@Override
	public void hide() {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void dispose() {
		stage.dispose();
		player.dispose();
	}
	
	private void createSample() {
		
		int toneLength = 0;
		try {
			toneLength = (int) (sample.sampleRate * (Float.parseFloat(timeField.getText())));
		} catch (NumberFormatException nfe) {
			Gdx.app.error("SGS", "User tried to set a time of " + timeField.getText(), nfe);
		}
		float toneFreq = frequencySlider.getValue();
		sample.sample.clear();
		sample.sample.ensureCapacity(toneLength);
		waveFormSelectBox.getSelected().generateWave(sample.sample, toneLength, toneFreq, sample.sampleRate);
		keepButton.setDisabled(false);
	};
	
	private ClickListener playButtonListener = new ClickListener() {
		@Override
		public void clicked(InputEvent event, float x, float y) {
			Gdx.app.log("Test", "Clicked");
			if (testReclickTime <= 0.0f) {
				createSample();
				// Bizarre GUI situation causes a need to abandon threads to the Aether sometimes
				// Hence the use of the re-click time, rather than a thread join
				testReclickTime = sample.sample.size / sample.sampleRate;
				Thread runner = new Thread(player);
				runner.start();
				
				playButton.setDisabled(true);
			}
		}
	};
	
	private ClickListener randomButtonListener = new ClickListener() {
		@Override
		public void clicked(InputEvent event, float x, float y) {
			Gdx.app.log("Random", "Clicked");
			// This code _relies_ on the slider updating the other actors
			waveFormSelectBox.setSelectedIndex(rand.nextInt(waveFormSelectBox.getItems().size));
			frequencySlider.setValue(rand.nextFloat() * (FREQUENCY_RANDOM_MAX - FREQUENCY_RANDOM_MIN) + FREQUENCY_RANDOM_MIN);
			timeField.setText(String.valueOf(rand.nextFloat() * (TIME_RANDOM_MAX - TIME_RANDOM_MIN) + TIME_RANDOM_MIN));
		};
	};

}
