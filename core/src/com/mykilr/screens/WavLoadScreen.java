package com.mykilr.screens;

import java.io.File;
import java.io.FileFilter;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.List;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.mykilr.SoundTestGame;
import com.mykilr.tools.PCMData;
import com.mykilr.tools.PCMUtils;

public class WavLoadScreen implements Screen {
	
	private static final String CANCEL           = "Cancel";
	private static final String OPEN             = "Open";
	
	private Color clearColor;
	private Color fadeColor;
	
	private Stage stage;
	private Table table;
	
	private PCMData data;
	
	private FileHandle currentPath;
	private FileHandle[] currentChildren;
	private List<String> folderContents;
	private ScrollPane listPane;
	private Label currentPathLabel;
	
	private TextButton openButton;
	private TextButton cancelButton;
	
	public WavLoadScreen(Skin skin, final SoundTestGame soundTestGame) {
		data = new PCMData();
		// Screen draw setup
		clearColor = Color.LIGHT_GRAY;
		fadeColor = new Color(Color.LIGHT_GRAY);
		fadeColor.a = 0.85f;
		
		// GUI
		stage = new Stage();
		table = new Table();
		
		currentPathLabel = new Label("", skin);
		folderContents = new List<String>(skin);
		listPane = new ScrollPane(folderContents);
		
		currentPath = Gdx.files.external("");
		changeDirectory(currentPath);
		
		openButton = new TextButton(OPEN, skin);
		cancelButton = new TextButton(CANCEL, skin);
		
		// Add behaviour mechanics
		openButton.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				if (openSelected(data)) soundTestGame.fileLoaded(true, data);
			}
		});
		cancelButton.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				soundTestGame.fileLoaded(false, data);
			}
		});
		folderContents.addListener(new ClickListener() {
			@Override
			public void touchUp(InputEvent event, float x, float y,
					int pointer, int button) {
				super.touchUp(event, x, y, pointer, button);
				fileSelected(folderContents.getSelectedIndex());
			}
		});
		
		// Layout
		table.add();
		table.add(currentPathLabel).colspan(3).align(Align.left);
		table.row();
		
		table.add();
		table.add(listPane).colspan(2).width(500).height(360);
		Table buttons = new Table();
		buttons.add(openButton).width(160);
		buttons.row();
		buttons.add(cancelButton).width(160);
		table.add(buttons).colspan(1);
		table.row();
		
		table.setFillParent(true);
		stage.addActor(table);
	}

	private void fileSelected(int selectedIndex) {
		FileHandle fileHandle = currentChildren[selectedIndex];
		if (fileHandle.isDirectory()){
			currentPathLabel.setText("/" + currentPath.path());
		} else {
			currentPathLabel.setText("/" + fileHandle.path());
		}
	}

	private boolean openSelected(PCMData data) {
		FileHandle child = currentChildren[folderContents.getSelectedIndex()];
		if (child.isDirectory()) {
			changeDirectory(child);
			return false;
		} else if (child.exists()) {
			PCMUtils.loadPCMFromWav(child, data);
			return true;
		}
		return false;
	}
	
	private void changeDirectory(FileHandle newDir) {
		if (newDir.isDirectory()) {
			currentPath = newDir;
			currentPathLabel.setText("/" + newDir.path());
			currentChildren = getWavListWithParent(newDir);
			folderContents.setItems(getStringArrayFromFileHandles(currentChildren));
		}
		
	}
	
	private FileHandle[] getWavListWithParent(FileHandle dir) {
	
		FileHandle[] tempList = dir.list(wavFilter);
		if (dir.parent().equals(dir)) {
			return tempList;
		}
		FileHandle[] retList = new FileHandle[tempList.length + 1];
		retList[0] = dir.parent();
		for (int i=0; i<tempList.length; i++) {
			retList[i+1] = tempList[i];
		}
		return retList;
	}
	
	FileFilter wavFilter = new FileFilter(){
		@Override
		public boolean accept(File pathname) {
			if (pathname.isHidden()) return false;
			if (pathname.getName().startsWith(".")) return false;
			if (pathname.isDirectory()) return true;
			if (pathname.getName().endsWith(".wav")) return true;
			return false;
		}
		
	};
		
	private String[] getStringArrayFromFileHandles(FileHandle[] handles) {
		String[] ret = new String[handles.length];
		for (int i=0; i<handles.length; i++) {
			String name = handles[i].name();
			ret[i] = (name.length() > 30 ? name.substring(0,  27) + "..." : name) +
				(handles[i].isDirectory() ? "/" : "");
		}
		// If the first item is the parent
		if (handles[0].equals(currentPath.parent())) {
			ret[0] = "../";
		}
		return ret;
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(clearColor.r, clearColor.g, clearColor.b,
				clearColor.a);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		// Draw GUI
		stage.act(delta);
		stage.draw();
	}

	@Override
	public void resize(int width, int height) {
		stage.getViewport().update(width, height, true);
	}

	@Override
	public void show() {
		changeDirectory(currentPath);
		Gdx.input.setInputProcessor(stage);
	}

	@Override
	public void hide() {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void dispose() {
		stage.dispose();
	}

}
