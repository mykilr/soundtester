package com.mykilr;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.mykilr.effects.Effect;
import com.mykilr.screens.EffectEditScreen;
import com.mykilr.screens.RecordingScreen;
import com.mykilr.screens.SampleGenerateScreen;
import com.mykilr.screens.ShowDFTScreen;
import com.mykilr.screens.SountTestScreen;
import com.mykilr.screens.WavLoadScreen;
import com.mykilr.screens.WavSaveScreen;
import com.mykilr.tools.PCMData;

public class SoundTestGame extends Game {

	private SountTestScreen       mainOptionsScreen;
	private SampleGenerateScreen  generateToneScreen;
	private RecordingScreen       recordSampleScreen;
	private WavLoadScreen         loadWaveFileScreen;
	private EffectEditScreen      effectEditScreen;
	private WavSaveScreen         saveWaveFileScreen;
	private ShowDFTScreen         showDFTScreen;

	// Gui
	private TextureAtlas atlas;
	private Skin skin;

	@Override
	public void create () {

		atlas = new TextureAtlas("gui/skins/simple/gui.atlas");
		skin = new Skin(Gdx.files.internal("gui/skins/simple/gui.json"), atlas);

		mainOptionsScreen       = new SountTestScreen(skin, this);
		generateToneScreen      = new SampleGenerateScreen(skin, this);
		recordSampleScreen      = new RecordingScreen(skin, this);
		loadWaveFileScreen      = new WavLoadScreen(skin, this);
		effectEditScreen        = new EffectEditScreen(skin, this);
		saveWaveFileScreen      = new WavSaveScreen(skin, this);
		showDFTScreen           = new ShowDFTScreen(skin, this);

		this.setScreen(mainOptionsScreen);
	}

	@Override
	public void dispose() {
		super.dispose();

		mainOptionsScreen.dispose();
		generateToneScreen.dispose();
		recordSampleScreen.dispose();
		loadWaveFileScreen.dispose();
		effectEditScreen.dispose();
		saveWaveFileScreen.dispose();
		showDFTScreen.dispose();

		atlas.dispose();
		skin.dispose();

	}

	public void generateTone() {
		this.setScreen(generateToneScreen);
	}

	public void recordSample() {
		this.setScreen(recordSampleScreen);
	}

	public void	loadWavFile() {
		this.setScreen(loadWaveFileScreen);
	}

	public void configEffect(Effect effect) {
		effectEditScreen.setEffect(effect);
		this.setScreen(effectEditScreen);
	}

	public void saveWavFile(PCMData outputData) {
		saveWaveFileScreen.setData(outputData);
		this.setScreen(saveWaveFileScreen);
	}

	public void showDFT(PCMData sample) {
		showDFTScreen.setSelectedPCMData(sample);
		this.setScreen(showDFTScreen);
	}

	public void toneGenerated(boolean toneCreated, PCMData sample) {
		if (toneCreated) {
			mainOptionsScreen.setSelectedPCMData(sample);
		}
		this.setScreen(mainOptionsScreen);
	}

	public void toneRecorded(boolean toneAccepted, PCMData sample) {
		if (toneAccepted) {
			mainOptionsScreen.setSelectedPCMData(sample);
		}
		this.setScreen(mainOptionsScreen);
	}

	public void fileLoaded(boolean fileLoaded, PCMData sample) {
		if (fileLoaded) {
			mainOptionsScreen.setSelectedPCMData(sample);
		}
		this.setScreen(mainOptionsScreen);
	}

	public void fileSaved() {
		this.setScreen(mainOptionsScreen);
	}

	public void effectConfigured(boolean keep, Effect effect, PCMData sample) {
		this.setScreen(mainOptionsScreen);
	}

	public void dftShown() {
		this.setScreen(mainOptionsScreen);
	}

}
