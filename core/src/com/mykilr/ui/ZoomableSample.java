package com.mykilr.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Widget;
import com.badlogic.gdx.scenes.scene2d.utils.DragListener;
import com.badlogic.gdx.utils.FloatArray;
import com.badlogic.gdx.utils.IntArray;
import com.badlogic.gdx.utils.ShortArray;

public class ZoomableSample extends Widget {
	
	private static final int PAN_SPEED = 5;
	private static final int MIN_SAMPLE_DRAW = 200;
	private static final int ZOOM_SPEED = 15;
	
	private ZoomableSampleStyle style;
	private ShapeRenderer renderer;
	private FloatArray sampleData;
	private float drawWidth;
	private float drawHeight;
	private int dataDrawStart;
	private int dataDrawLength;
	public IntArray markers;
	
	public ZoomableSample(ShortArray sampleData, Skin skin) {
		this(sampleData, skin.get(ZoomableSampleStyle.class));
	}

	public ZoomableSample(ShortArray sample, ZoomableSampleStyle style) {
		super();
		sampleData = new FloatArray(); 
		setStyle(style);
		setSampleData(sample);
		fullyExpandZoom();
		renderer = new ShapeRenderer();
		markers = new IntArray();
		
		// Test Data
		setDrawWidth(1000.0f);
		setDrawHeight(100.0f);
		
		addListener(new ZoomableSampleGestureListener(this));
	}
	
	public void fullyExpandZoom() {
		dataDrawStart = 0;
		if (sampleData != null) {
			dataDrawLength = sampleData.size;
		}
	}
	
	@Override
	public float getPrefWidth () {
		return getDrawWidth();
	}

	@Override
	public float getPrefHeight () {
		return getDrawHeight();
	}
	
	@Override
	public void draw(com.badlogic.gdx.graphics.g2d.Batch batch,
			float parentAlpha) {
		
		super.draw(batch, parentAlpha);
		// Disable batch temporarily while we do lines
		batch.end();

		// Draw in the same projection as the batch though
		renderer.setProjectionMatrix(batch.getProjectionMatrix());
		
		// Start the lines tool
		renderer.begin(ShapeType.Line);
		
		// Set the colour for drawing the axes
		if (style != null && style.axisColor != null) {
			renderer.setColor(style.axisColor);
		}
		
		// Need some basic limits
		float h = this.getHeight();
		float w = this.getWidth();
		float x = this.getX();
		float y = this.getY();
		
		// Draw axes
		renderer.line(x, y + h / 2, x + w, y + h / 2);
		
		if (sampleData != null) {
			// Set the colour for drawing the axes
			if (style != null && style.zoomColor != null) {
				renderer.setColor(style.zoomColor);
			}
			// Draw "subtle" zoom position indicator
			float zoomStartX = dataDrawStart * w / sampleData.size;
			float zoomEndX = (dataDrawStart + dataDrawLength) * w / sampleData.size;
			renderer.line(x + zoomStartX, y, x + zoomEndX, y);
			
			// Draw Lines between sample points using the line colour
			if (style != null && style.plotColor != null) {
				renderer.setColor(style.plotColor);
			}
			
			// Only draw lines if we've something to draw
			if (sampleData.size > 0) {
				float pointAX = x;
				float datumA = sampleData.get(dataDrawStart);
				// datum is drawn scaled from (-Short.MAX:+Short.Max) to (0:h) 
				float pointAY = y + datumA * h;
				float dX = w / (float)dataDrawLength;
				
				// Draw lines between each point
				for (int bInd = 1; bInd < dataDrawLength; bInd++) {
					float datumB = sampleData.get(dataDrawStart + bInd);
					float pointBX = pointAX + dX;
					float pointBY = y + datumB * h;
					renderer.line(pointAX, pointAY, pointBX, pointBY);
					pointAX = pointBX;
					pointAY = pointBY;
				}
				
				// Draw any vertical markers
				if (style != null && style.markerColor != null) {
					renderer.setColor(style.markerColor);
				}
				
				for (int i = 0; i < markers.size; i++) {
					// If this marker is in the drawn range
					int mark = markers.get(i);
					if (mark >= dataDrawStart && mark <= (dataDrawLength + dataDrawStart)) {
						float drawX = x + (mark - dataDrawStart) * dX;
						renderer.line(drawX, y, drawX, y + h);
					}
				}
			}
		}
		
		// Finished with the lines
		renderer.end();
		
		// Need batch online again, we want to do some icons
		batch.begin();
	};

	public void setSampleData(ShortArray shortData) {
		sampleData.clear();
		sampleData.ensureCapacity(shortData.size);
		for (int i=0; i<shortData.size; i++) {
			sampleData.add(((float)(shortData.get(i)) + Short.MAX_VALUE) / (2f * Short.MAX_VALUE));
		}
		fullyExpandZoom();
	}
	
	public void setSampleData(FloatArray sampleData) {
		this.setSampleData(sampleData, 1f);
	}
	
	public void setSampleData(FloatArray sampleData, float scale) {
		this.sampleData.clear();
		this.sampleData.addAll(sampleData);
		for (int i=0; i < this.sampleData.size; i++) {
			
			this.sampleData.items[i] *= scale;
			this.sampleData.items[i] += 0.5f;
		}
		fullyExpandZoom();
	}
	
	private class ZoomableSampleGestureListener extends DragListener {
		private ZoomableSample zoomSample;
		private boolean multiTouched;
		private int xPos;
		private int xDiff;
		
		public ZoomableSampleGestureListener(ZoomableSample zoomableSample) {
			this.zoomSample = zoomableSample;
			multiTouched = false;
			xDiff = 0;
		}
		
		@Override
		public void drag(InputEvent event, float x, float y, int pointer) {
			int lastXDiff = xDiff;
			int lastXPos = xPos;
			updateMultiTouch();
			if (multiTouched) {
				int xMid = (Gdx.input.getX(0) + Gdx.input.getX(1)) / 2;
				zoomSample.pinch(lastXDiff - xDiff, xMid);
			} else {
				zoomSample.swipe(lastXPos - xPos);
			}
		}

		@Override
		public boolean touchDown(InputEvent event, float x, float y,
				int pointer, int button) {
			updateMultiTouch();
			// Gdx.app.log("ZS", "TouchDown (" + x + ", " + y + "), pointer: " + pointer + ", button: " + button);
			return super.touchDown(event, x, y, pointer, button);
		}
		
		private void updateMultiTouch() {
			multiTouched = Gdx.input.isTouched(0) && Gdx.input.isTouched(1);
			if (multiTouched) {
				// Should we process to stage/widget coordinates?
				xDiff = Math.abs(Gdx.input.getX(0) - Gdx.input.getX(1));
			} else {
				xDiff = 0;
				xPos = Gdx.input.getX(0);
			}
		}

	}
	
	private void pinch(int pinchDist, int xMid) {
		if (sampleData == null || sampleData.size == 0) return;
		// positive pinch is bringing finger tips together
		// Gdx.app.log("ZS", "pinch(" + pinchDist + ")");
		if (pinchDist > 0) {
			// check we can zoom out further
			zoomOut(pinchDist);
		} else if (pinchDist < 0) {
			// check there are at least 2 data left to draw if we zoom
			zoomIn(xMid, -pinchDist);
		}
	}
	
	private void zoomIn(int x, int zoomAmount) {
		// Ignore the x for now, just zoom on the middle
		// Check plausible limits when zooming
		int newDataDrawLength = dataDrawLength - (zoomAmount * ZOOM_SPEED);
		if (newDataDrawLength > MIN_SAMPLE_DRAW) {
			dataDrawLength = newDataDrawLength;
			int newDataDrawStart = dataDrawStart + (zoomAmount * ZOOM_SPEED / 2);
			newDataDrawStart = newDataDrawStart > 0 ? newDataDrawStart : 0;
			dataDrawStart = (newDataDrawStart + dataDrawLength) < sampleData.size
					? newDataDrawStart : sampleData.size - dataDrawLength;
		} else {
			dataDrawLength = MIN_SAMPLE_DRAW;
		}
		
	}

	private void zoomOut(int zoomAmount) {
		// Ignore the x, just zoom out from the middle
		int newDataDrawLength = dataDrawLength + (zoomAmount * ZOOM_SPEED);
		int newDataDrawStart = dataDrawStart - (zoomAmount * ZOOM_SPEED / 2);
		
		// Check plausible limits when zooming
		dataDrawLength = newDataDrawLength < sampleData.size ? newDataDrawLength : sampleData.size;
		newDataDrawStart = newDataDrawStart > 0 ? newDataDrawStart : 0;
		dataDrawStart = (newDataDrawStart + dataDrawLength) < sampleData.size
				? newDataDrawStart : sampleData.size - dataDrawLength;
	}

	private void swipe(int swipeDist) {
		if (sampleData == null || sampleData.size == 0) return;
		// positive swipe is to the right
		// Gdx.app.log("ZS", "swipe(" + swipeDist + ")");
		
		if (swipeDist > 0) {
			// check we can move the draw start to the left (decrease)
			panLeft(swipeDist);
		} else if (swipeDist < 0.) {
			// check we can move the draw start to the right (increase)
			panRight(-swipeDist);
		}
	}

	private void panRight(int panDist) {
		// pan as a portion of the total drawn length
		int newDataDrawStart = dataDrawStart - panDist * PAN_SPEED * dataDrawLength / sampleData.size;
		dataDrawStart = newDataDrawStart > 0 ? newDataDrawStart : 0;
	}

	private void panLeft(int panDist) {
		// pan as a portion of the total drawn length
		int newDataDrawStart = dataDrawStart + panDist * PAN_SPEED * dataDrawLength / sampleData.size;
		dataDrawStart = (newDataDrawStart + dataDrawLength) < sampleData.size
				? newDataDrawStart : sampleData.size - dataDrawLength;
	}

	static public class ZoomableSampleStyle {
		public Color axisColor;
		public Color plotColor;
		public Color zoomColor;
		public Color markerColor;
		
		public ZoomableSampleStyle() {
		}

		public ZoomableSampleStyle(Color axisColor, Color plotColor, Color zoomColor, Color markerColor) {
			this.axisColor = axisColor;
			this.plotColor = plotColor;
			this.zoomColor = zoomColor;
			this.markerColor = markerColor;
		}
		
		public ZoomableSampleStyle(ZoomableSampleStyle style) {
			if (style.axisColor != null)
				axisColor = new Color(style.axisColor);
			if (style.plotColor != null)
				plotColor = new Color(style.plotColor);
			if (style.zoomColor != null)
				zoomColor = new Color(style.zoomColor);
			if (style.markerColor != null)
				zoomColor = new Color(style.markerColor);
		}
		
	}
	
	public ZoomableSampleStyle getStyle() {
		return style;
	}

	public void setStyle(ZoomableSampleStyle style) {
		this.style = style;
	}

	public float getDrawWidth() {
		return drawWidth;
	}

	private void setDrawWidth(float drawWidth) {
		this.drawWidth = drawWidth;
	}

	private float getDrawHeight() {
		return drawHeight;
	}

	private void setDrawHeight(float drawHeight) {
		this.drawHeight = drawHeight;
	}
}
