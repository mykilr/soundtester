package com.mykilr.ui;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.SelectBox;
import com.badlogic.gdx.scenes.scene2d.ui.Slider;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.mykilr.tools.Note;

public class CrossUpdateListener extends ChangeListener implements TextField.TextFieldListener {
	
	private Type sourceType;
	private Object source;
	private Type destType;
	private Object dest;
	
	private String floatFormat = "%.2f";
	
	/**
	 * @param floatFormat the floatFormat to set
	 */
	public void setFloatFormat(String floatFormat) {
		this.floatFormat = floatFormat;
	}

	public CrossUpdateListener(SelectBox<Note> source, TextField dest) {
		this.sourceType = Type.BOX_NOTE;
		this.destType = Type.TEXT_FLOAT;
		this.source = source;
		this.dest = dest;
	}
	
	public CrossUpdateListener(SelectBox<Note> source, Slider dest) {
		this.sourceType = Type.BOX_NOTE;
		this.destType = Type.SLIDER_FLOAT;
		this.source = source;
		this.dest = dest;
	}
	
	public CrossUpdateListener(TextField source, SelectBox<Note> dest) {
		this.sourceType = Type.TEXT_FLOAT;
		this.destType = Type.BOX_NOTE;
		this.source = source;
		this.dest = dest;
	}
	
	public CrossUpdateListener(TextField source, Slider dest) {
		this.sourceType = Type.TEXT_FLOAT;
		this.destType = Type.SLIDER_FLOAT;
		this.source = source;
		this.dest = dest;
	}
	
	public CrossUpdateListener(Slider source, SelectBox<Note> dest) {
		this.sourceType = Type.SLIDER_FLOAT;
		this.destType = Type.BOX_NOTE;
		this.source = source;
		this.dest = dest;
	}
	
	public CrossUpdateListener(Slider source, TextField dest) {
		this.sourceType = Type.SLIDER_FLOAT;
		this.destType = Type.TEXT_FLOAT;
		this.source = source;
		this.dest = dest;
	}

	@SuppressWarnings("unchecked") // We know the object types
	@Override
	public void changed(ChangeEvent event, Actor actor) {
		// Get value to transfer
		float value = 0.0f;
		switch(sourceType){
		case BOX_NOTE:
			value = ((SelectBox<Note>)source).getSelected().freq;
			break;
		case SLIDER_FLOAT:
			value = ((Slider)source).getValue();
			break;
		case TEXT_FLOAT:
			try {
				value = Float.parseFloat("0" + ((TextField)source).getText());
			} catch (NumberFormatException nfe) {
				value = 0.0f;
			}
			break;
		default:
			break;
		}
		// Update destination
		switch(destType) {
		case BOX_NOTE:
			((SelectBox<Note>)dest).setSelected(Note.nearest(value));
			break;
		case SLIDER_FLOAT:
			((Slider)dest).setValue(value);
			break;
		case TEXT_FLOAT:
			((TextField)dest).setText(String.format(floatFormat, value));
			break;
		default:
			break;
		}
	}

	@Override
	public void keyTyped(TextField textField, char c) {
		// The slider can trigger events when updated
		// To counter this store the text and position
		String text = textField.getText();
		int pos = textField.getCursorPosition();
		changed(null, null);
		// Restore the text and position in case it 
		// was modified by a slider event
		textField.setText(text);
		textField.setCursorPosition(pos);
		
	}
	
	/* Listener Configuration */
	enum Type {
		BOX_NOTE,
		TEXT_FLOAT,
		SLIDER_FLOAT
	};
	
}