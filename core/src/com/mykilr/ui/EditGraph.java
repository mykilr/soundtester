package com.mykilr.ui;

import java.util.Random;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Widget;
import com.badlogic.gdx.scenes.scene2d.utils.DragListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.utils.Array;

public class EditGraph extends Widget {
	
	private EditGraphStyle style;
	private ShapeRenderer renderer;
	// Graphical dimensions
	private float drawWidth;
	private float drawHeight;
	// Data dimensions
	private float dataMaxX = 1.0f;
	private float dataMaxY = 1.0f;
	// Data storage
	private Array<Vector2> points;
	private int selectedPoint;
	// Optional end points
	private boolean originPoint;
	private boolean topYZeroPoint;
	// Optional x axis restriction
	private boolean restrictX;

	public EditGraph(Skin skin) {
		this(skin.get(EditGraphStyle.class));
	}
	
	public EditGraph(EditGraphStyle style) {
		super();
		setStyle(style);
		renderer = new ShapeRenderer();
		points = new Array<Vector2>();
		// Test point
		originPoint = true;
		topYZeroPoint = true;
		drawWidth = 500.0f;
		drawHeight = 300.0f;
		restrictX = true;
		addPoint(new Vector2(0.25f, 1.0f));
		addPoint(new Vector2(0.5f, 0.5f));
		addPoint(new Vector2(0.75f, 0.5f));
		selectedPoint = 1;
		
		addListener(new EditGraphDragListener(this));
	}
	
	public void addPoint(Vector2 point) {
		if (restrictX && points.size > 0) {
			if (points.peek().x > point.x) return;
		}
		points.add(point);
	}
	
	public void selectPoint(int index) {
		if (index >= 0 && index < points.size) {
			selectedPoint = index;
		}
	}
	
	public void moveSelectedPoint(float pixelX, float pixelY) {
		if (selectedPoint >= 0 && selectedPoint < points.size) {
			
			float dataX = pixelX * dataMaxX / getWidth();
			float dataY = pixelY * dataMaxY / getHeight();
			
			if (restrictX) {
				// Don't move the point past other points on the graph
				if (selectedPoint > 0 && points.get(selectedPoint - 1).x > dataX) {
					dataX = points.get(selectedPoint - 1).x;
				}
				if (selectedPoint < points.size - 1 && points.get(selectedPoint + 1).x < dataX) {
					dataX = points.get(selectedPoint + 1).x;
				}
			}
			
			if (dataX < 0.0f) dataX = 0.0f;
			if (dataX > dataMaxX) dataX = dataMaxX;
			if (dataY < 0.0f) dataY = 0.0f;
			if (dataY > dataMaxY) dataY = dataMaxY;
			
			
			points.get(selectedPoint).set(dataX, dataY);
		}
	}

	public void selectNearest(float pixelX, float pixelY) {
		
		float dataX = pixelX * dataMaxX / getWidth();
		float dataY = pixelY * dataMaxY / getHeight();
		
		int nearest = -1;
		float sqrDst = dataMaxX * dataMaxY;
		for (int ind = 0; ind < points.size; ind++) {
			float pointSqrDst = points.get(ind).dst2(dataX, dataY);
			if (pointSqrDst < sqrDst) {
				sqrDst = pointSqrDst;
				nearest = ind;
			}
		}
		selectPoint(nearest);
	}

	@Override
	public void act(float delta) {
		super.act(delta);
		// Gdx.app.log("EG", "act called");
	};
	
	@Override
	public void layout() {
		super.layout();
		
	};

	@Override
	public void draw(com.badlogic.gdx.graphics.g2d.Batch batch,
			float parentAlpha) {
		
		super.draw(batch, parentAlpha);
		// Disable batch temporarily while we do lines
		batch.end();

		// Draw in the same projection as the batch though
		renderer.setProjectionMatrix(batch.getProjectionMatrix());
		
		// Start the lines tool
		renderer.begin(ShapeType.Line);
		
		// Set the colour for drawing the axes
		if (style != null && style.axisColor != null) {
			renderer.setColor(style.axisColor);
		}
		
		// Need some basic limits
		float h = this.getHeight();
		float w = this.getWidth();
		float x = this.getX();
		float y = this.getY();
		
		// Draw axes
		renderer.line(x, y, x, y + h);
		renderer.line(x, y, x + w, y);
		
		// Draw Lines between points using the line colour
		if (style != null && style.plotColor != null) {
			renderer.setColor(style.plotColor);
		}
		
		// Only draw lines if we've something to draw
		if (points.size > 0) {
			float pointAX = points.first().x * w / dataMaxX + x;
			float pointAY = points.first().y * h / dataMaxY + y;
			
			// Draw from origin to first point
			if (originPoint) {
				renderer.line(x, y, pointAX, pointAY);
			}
			
			// Draw lines between each point
			for (int bInd = 1; bInd < points.size; bInd++) {
				float pointBX = points.get(bInd).x * w / dataMaxX + x;
				float pointBY = points.get(bInd).y * h / dataMaxY + y;
				renderer.line(pointAX, pointAY, pointBX, pointBY);
				pointAX = pointBX;
				pointAY = pointBY;
			}
			
			// Draw from last point to top of axis
			if (topYZeroPoint) {
				renderer.line(pointAX,  pointAY, x + w, y);
			}
		}
		// Finished with the lines
		renderer.end();
		
		// Need batch online again, we want to do some icons
		batch.begin();
		
		// Draw icons at each point location
		for (int ind = 0; ind < points.size; ind++) {
			float pointAX = points.get(ind).x * w / dataMaxX + x;
			float pointAY = points.get(ind).y * h / dataMaxY + y;
			
			if (selectedPoint == ind && style.selectedPoint != null) {
				// If this is the selected point and we have an icon for that
				float width = style.selectedPoint.getMinWidth();
				float height = style.selectedPoint.getMinHeight();
				
				// draw selected icon
				style.selectedPoint.draw(batch, pointAX - width / 2, pointAY - height / 2, width, height);
			} else if (style.point != null) {
				// If there's no selected Icon, or this is not the selected point
				float width = style.point.getMinWidth();
				float height = style.point.getMinHeight();
				
				// draw normal icon
				style.point.draw(batch, pointAX - width / 2, pointAY - height / 2, width, height);
			}
			// If no icons are set then simply don't draw them
		}
	};

	@Override
	public float getPrefWidth () {
		return drawWidth;
	}

	@Override
	public float getPrefHeight () {
		return drawHeight;
	}
	
	private class EditGraphDragListener extends DragListener {
		
		private EditGraph graph;
		
		private EditGraphDragListener(EditGraph graph) {
			this.graph = graph;
		}
		
		@Override
		public void drag (InputEvent event, float x, float y, int pointer) {
			graph.moveSelectedPoint(x, y);
		}
		
		@Override
		public boolean touchDown(InputEvent event, float x, float y,
				int pointer, int button) {
			graph.selectNearest(x, y);
			return super.touchDown(event, x, y, pointer, button);
		}
	}

	/**
	 * The style for a EditGraph, see {@link Label}.
	 * 
	 * @author Kurley
	 */
	static public class EditGraphStyle {
		public Color axisColor;
		public Color plotColor;
		public Drawable point;
		public Drawable selectedPoint;

		public EditGraphStyle() {
		}

		public EditGraphStyle(Color axisColor, Color plotColor, 
				Drawable point, Drawable selectedPoint) {
			this.axisColor = axisColor;
			this.plotColor = plotColor;
			this.point = point;
			this.selectedPoint = selectedPoint;
		}

		public EditGraphStyle(EditGraphStyle style) {
			if (style.axisColor != null)
				axisColor = new Color(style.axisColor);
			if (style.plotColor != null)
				plotColor = new Color(style.plotColor);
			point = style.point;
			selectedPoint = style.selectedPoint;
		}
	}
	
	public EditGraphStyle getStyle() {
		return style;
	}

	public void setStyle(EditGraphStyle style) {
		this.style = style;
	}

	public Array<Vector2> getPoints() {
		return points;
	}

	public void randomRepositioning(Random rand) {
		for (int ind = 0; ind < points.size; ind++) {
			points.get(ind).x = (float) (0.25f + ind * 0.25f + rand.nextGaussian() * 0.05f);
			points.get(ind).y = (float) (0.5f + rand.nextGaussian() * 0.25f);
		}
	}
	
}
