package com.mykilr.tools;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.FloatArray;
import com.mykilr.screens.ShowDFTScreen;

public class Transform {

	private static final double PI = Math.PI;

	/**
	 * 
	 * @param dft
	 * @param offset
	 * @param n
	 * @param inverse
	 * @return scale of the dft.real (maximum absolute value);
	 */
	public static float fastFourierTransform(PCMDomain dft, int offset, int n, boolean inverse) {
			
			// Setting some local constants
			final int nm1 = n - 1;
			final int nd2 = n / 2;
			final int m = (int) (Math.log(n) / Math.log(2));
			
			// Some local variables
			int j;
			int k;
			float tr; // temp real
			float ti; // temp imaginary
			float ur; 
			float ui;
			float sr; // Sinusoid real
			float si; // Sinusoid imaginary
			
	//		progressStage += 1;
			j = nd2;
			// Bit reversal sorting
			for (int i = 1; i <= n-2; i++) {
	//			if (stop) return;
	//			progressUpdate = (100 * (i-2)) / nm1;
	//			applyProgressUpdate = true;
				if (!(i >= j)) {
					tr = dft.real.items[j + offset];
					ti = dft.imaginary.items[j + offset];
					dft.real.items[j + offset] = dft.real.items[i + offset];
					dft.imaginary.items[j + offset] = dft.imaginary.items[i + offset];
					dft.real.items[i + offset] = tr;
					dft.imaginary.items[i + offset] = ti;
				}
				k = nd2;
				while (!(k > j)) {
	//				if (stop) return;
					j = j - k;
					k = k / 2;
					if (k == 0 && j == 0) break;
				}
				j = j + k;
			}
	
	//		progressStage += 1;
	//		applyProgressUpdate = true;
			
			// Stage loop
			for (int l = 1; l <= m; l++) {
	//			if (stop) return;
	//			progressUpdate = (100 * (l-1)) / m;
	//			applyProgressUpdate = true;
				int le = (int) Math.pow(2, l);
				int le2 = le / 2;
				ur = 1;
				ui = 0;
				sr = (float) Math.cos(ShowDFTScreen.PI / le2);
				si = (float) Math.sin(ShowDFTScreen.PI / le2) * (inverse ? 1 : -1);
				// Sub DFT loop
				for (j = 1; j <= le2; j++) {
					int jm1 = j - 1;
					// Loop for each FFT Butterfly
					for (int i = jm1; i <= nm1; i += le) {
	//					if (stop) return;
						int ip = i + le2;
						if (ip >= n || i >= n) continue; 
						tr = dft.real.items[ip + offset] * ur - dft.imaginary.items[ip + offset] * ui;
						ti = dft.real.items[ip + offset] * ui + dft.imaginary.items[ip + offset] * ur;
						dft.real.items[ip + offset]      = dft.real.items[i + offset]      - tr;
						dft.imaginary.items[ip + offset] = dft.imaginary.items[i + offset] - ti;
						dft.real.items[i + offset]      += tr;
						dft.imaginary.items[i + offset] += ti;
					}
					float tur = ur;
					ur = tur * sr - ui * si;
					ui = tur * si + ui * sr;
				}
			}
			
			//  Scale check
			float min = Float.MAX_VALUE;
			float max = Float.MIN_NORMAL;
			for (int i = 0; i < dft.real.size; i++) {
				float f = dft.real.get(i);
				min = (min < f ? min : f);
				max = (max > f ? max : f);
			}
			return Math.max(Math.abs(min), Math.abs(max));
	
	//		progressUpdate = 100f;
	//		applyProgressUpdate = true;
		}

	public static int nextPowerOf2(final int a) {
		int b = 1;
		while (b < a) {
			b = b << 1;
		}
		return b;
	}

	public static int lastPowerOf2(final int a) {
		return nextPowerOf2(a) >> 1;
	}
	
	public static int copyShortSampleToFloatDomain(PCMData sample,
			PCMDomain dft) {
		
		return copyShortSampleToFloatDomain(sample, dft, 0, sample.sample.size);
	}

	public static int copyShortSampleToFloatDomain(PCMData sample,
			PCMDomain dft, int offset, int len) {
		int snipSize = nextPowerOf2(len);
		copyShortSampleToFloatDomain(sample, dft, offset, len, snipSize, 0);
		return snipSize;
	}

	public static void copyShortSampleToFloatDomain(PCMData sample,
			PCMDomain dft, int srcOffset, int srcLen, int windowSize, int dstPrefix) {
		
		dft.padding =  windowSize - srcLen - dstPrefix;

		// Start with zeroed arrays
		dft.real.clear();
		dft.imaginary.clear();
		dft.real.ensureCapacity(srcLen + dft.padding);
		dft.imaginary.ensureCapacity(srcLen + dft.padding);
		
		// Add dst offset (padding) prefix as zeroes
		for (int i = 0; i < dstPrefix; i++) {
			dft.real.add(0.0f);
			dft.imaginary.add(0.0f);
		}
		
		// Annoying copy from short to float
		for (int i = 0; i < srcLen; i++) {
			dft.real.add(sample.sample.items[i + srcOffset]);
			dft.imaginary.add(0.0f);
		}
		
		for (int i = srcLen; i < srcLen + dft.padding; i++) {
			dft.real.add(0.0f);
			dft.imaginary.add(0.0f);
		}
		
		Gdx.app.log("cpStF", "dft.real.size = " + dft.real.size);
	}

	public static void pitchShift(PCMDomain dft, int sampleShift) {
		Gdx.app.log("Tfm","dft.real.size = " + dft.real.size);
		
//		dft.updatePolarFromRectangular();
//		
//		pitchShift(dft.magnitude, sampleShift);
//		pitchShift(dft.phase, sampleShift);
//		
//		dft.updateRectangularFromPolar();
		
		pitchShift(dft.real, sampleShift);
		pitchShift(dft.imaginary, sampleShift);
		
	}

	
	/**
	 * Shift the (presumably frequency domain) data towards or away from the center
	 * Fill the gap with zeroes
	 * @param data
	 * @param shift
	 */
	private static void pitchShift(FloatArray data, int shift) {
		if (data == null || data.size == 0) return;

		// Away from the center (negative shift)
		if (shift < 0) {
			for (int i = 0; i < data.size / 2 + shift ; i++) {
				int i2 = data.size - 1 - i;
				data.set(i, data.get(i - shift));
				data.set(i2, data.get(i2 + shift));
			}
			for (int i = data.size / 2 + shift; i <= data.size / 2 - shift; i++) {
				data.set(i, 0);
			}
		}
		// Towards the center (positive shift)
		if (shift > 0) {
			for (int i = data.size / 2 - 1; i >= shift; i--) {
				int i2 = data.size - 1 - i;
				data.set(i, data.get(i - shift));
				data.set(i2, data.get(i2 + shift));
			}
			for (int i = 0; i >= shift; i++) {
				int i2 = data.size - 1 - i;
				data.set(i, 0);
				data.set(i2, 0);
			}
		}
	}
	
	public static void copyFloatDomainToShortSample(PCMDomain dft,
			PCMData sample) {
		
		float scale = 1.0f;
		copyFloatDomainToShortSample(dft, sample, scale);
	}

	public static void copyFloatDomainToShortSample(PCMDomain dft,
			PCMData sample, float scale) {
		
		int offset = 0;
		int n = dft.real.size - dft.padding;
		
		copyFloatDomainToShortSample(dft.real, sample, scale, offset, n, 0);
	}

	public static void copyFloatDomainToShortSample(FloatArray dft,
			PCMData sample, float scale, int dstOffset, int dstLen, int srcPrefix) {
		if (scale == 0.0f) scale = 1.0f;
		
		// sample.sample.clear();
		sample.sample.ensureCapacity(dstLen - sample.sample.size);
		
		for (int i = 0; i < dstLen; i++) {
			if (i + dstOffset >= sample.sample.size) sample.sample.add(0);
			int newValue = (short) ((dft.get(i + srcPrefix) / scale) * Short.MAX_VALUE);
			newValue = (newValue > Short.MAX_VALUE) ? Short.MAX_VALUE : newValue;
			newValue = (newValue < Short.MIN_VALUE) ? Short.MIN_VALUE : newValue;
			sample.sample.set(i + dstOffset, (short)newValue);
		}
	}
	
	public static void mergeFloatDomainOnToShortSample(FloatArray dft,
			PCMData sample, float scale, int dstOffset, int dstLen, int srcPrefix) {
		if (scale == 0.0f) scale = 1.0f;
		
		// sample.sample.clear();
		sample.sample.ensureCapacity(dstLen - sample.sample.size);
		
		for (int i = 0; i < dstLen; i++) {
			if (i + dstOffset >= sample.sample.size) sample.sample.add(0);
			int newValue = sample.sample.get(i + dstOffset) + (int)((dft.get(i + srcPrefix) / scale) * Short.MAX_VALUE);
			newValue = (newValue > Short.MAX_VALUE) ? Short.MAX_VALUE : newValue;
			newValue = (newValue < Short.MIN_VALUE) ? Short.MIN_VALUE : newValue;
			sample.sample.set(i + dstOffset, (short) newValue);
		}
	}

	public static void applyHanningWindow(FloatArray data) {
		applyAdjustedHanningWindow(data, 0.5f, 0f);
//		applyBartlettWindow(data);
	}	
	
	public static void applyBartlettWindow(FloatArray data) {
		for (int i = 0; i < data.size; i++) {
			// bartlett(x) = abs(1-abs((x - len / 2) / (len / 2)))
			float scale = 1f;
			if (i < data.size / 2) {
				scale = (float)i / (float)(data.size / 2);
			} else {
				scale = (float)(data.size - i) / (float)(data.size / 2);
			}
			float scaledValue = data.get(i) * scale;
			
			
			data.set(i, scaledValue);
		}
	}
	
	public static void applyAdjustedHanningWindow(FloatArray data, float scale, float height) {
		for (int i = 0; i < data.size; i++) {
			// hann(x) = 0.5 * (1 - cos(4 * pi * x/ range))
			float scaledValue = (float) (data.get(i) * (scale * ((1f + height) - Math.cos(2f * PI * (float)i / (float)data.size))));
			
			
			data.set(i, scaledValue);
		}
	}

	/** Modifies the given sample's amplitude by the graph described by points.
	 * Each vector in the point represents a time (x) and an amplitude (y) where
	 * x is 0.0f to 1.0f from the start to the end of the sample, and 
	 * y is 0.0f to 1.0f representing silence to the loudest amplitude.
	 * 
	 * @param sample
	 * @param points
	 * @param sampleRate
	 */
	public static void applyAmplitudeEnvelope(FloatArray sample, Array<Vector2> points) {
		int partLen = 0;
		float amp = 0.0f; // Starting amplitude
		float ampD = 0.0f;
		int t0 = 0; // Sample time of first graph point
		
		for (int part = 0; part < points.size; part++) {
			// partLen is the distance between 2 graph points in time samples
			partLen = (int) Math.floor(sample.size * points.get(part).x) - t0;
			// ampD is the amount the amplitude changes per sampleDatum
			ampD = partLen > 0 ? (points.get(part).y - amp) / partLen : 0;
			// change the amplitude for all the data between the 2 graph points
			for (int t = 0; t < partLen; t++) {
				// modify the amplitude for each datum
				sample.set(t0 + t, (sample.get(t0 + t) * (amp + ampD * t)));
			}
			// Move the time up to the next slope
			t0 += partLen;
			// Move the base amplitude to that of the next starting point
			amp = points.get(part).y;
		}
		// If there is still sample time left, do a full drop to silence
		partLen = sample.size - t0;
		Gdx.app.log("PMCU", "partLen: " + partLen);
		// The last ampD should bring the data to zero
		ampD = partLen > 0 ? (0.0f - amp) / partLen : 0;
		Gdx.app.log("PMCU", "ampD: " + ampD);
		for (int t = 0; t < partLen; t++) {
			sample.set(t0 + t, (sample.get(t0 + t) * (amp + ampD * t)));
		}
	}

}
