package com.mykilr.tools;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.AudioDevice;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

public class NoisePlayer implements Runnable {
	
	private class PlayerListener extends ClickListener {
		private NoisePlayer player;
		private Thread playThread;
		
		PlayerListener(NoisePlayer player) {
			this.player = player;
		}
		
		@Override
		public void clicked(InputEvent event, float x, float y) {
			super.clicked(event, x, y);
			play();
		}

		private void play() {
			if (!player.stop) {
				player.stop();
				try {
					playThread.join();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				playThread = null;
			} else {
				playThread = new Thread(player);
				playThread.start();
			}
		}
	}
	
	private static final int PLAY_SAMPLES = 1024;
	private static final short[] emptyBuffer = new short[PLAY_SAMPLES];
	private AudioDevice audioDev;
	private boolean stop;
	private PCMData buffer;
	private PlayerListener listener;
	private TextButton playButton;
	private String playText = "";
	private String stopText = "";
	
	public NoisePlayer (PCMData buffer) {
		this.stop = true;
		this.buffer = buffer;
		this.listener = new PlayerListener(this);
	}
	
	@Override
	public void run() {
		// don't play an empty buffer
		if (buffer == null) {
			stop = true;
		} else {
			stop = false;
			// Will probably have a play button, but check anyway
			if (playButton != null) {
				playButton.setText(stopText);
			}
			// Play only what we need to
			int playSamples = PLAY_SAMPLES;
			int numSamples = Math.min(playSamples, buffer.sample.size);
			int offset = 0;
			// Try to play the sample in a non-blocking manner
			audioDev = Gdx.audio.newAudioDevice(buffer.sampleRate, buffer.channels == 1);
			audioDev.writeSamples(emptyBuffer, 0, emptyBuffer.length);
			while (!stop) {
//				Gdx.app.log("PLAY", "Writing array of length " + numSamples);
				audioDev.writeSamples(buffer.sample.items, offset, numSamples);
				offset += numSamples;
				numSamples = Math.min(numSamples, buffer.sample.size - offset);
				if (numSamples == 0) stop = true;
			}
			audioDev.writeSamples(emptyBuffer, 0, emptyBuffer.length);
			audioDev.dispose();
			if (playButton != null) {
				playButton.setText(playText);
			}
		}
	}
	
	public TextButton getPlayStopButton(String playText, String stopText, Skin skin) {
		this.playText = playText;
		this.stopText = stopText;
		if (playButton != null) {
			playButton.setText(stop == true ? playText : stopText);
		} else {
			playButton = new TextButton(stop == true ? playText : stopText, skin);
			playButton.addListener(listener);
		}
		return playButton;
	}
	
	public void stop() {
		stop = true;
	}

	public void dispose() {
	}

	public void setSampleData(PCMData sample) {
		this.buffer = sample;
	}
}