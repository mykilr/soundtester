package com.mykilr.tools;

import java.util.Random;

import com.badlogic.gdx.utils.ShortArray;

public class WaveForm {
	
	private String name;
	private Generator generator;
	
	private interface Generator {
		public void generateWave(ShortArray sample, int toneLength, 
				float toneFreq, int sampleRate);
	}

	private WaveForm(String name, Generator generator) {
		this.name = name;
		this.generator = generator;
	}
	
	public void generateWave(ShortArray sample, int toneLength, 
			float toneFreq, int sampleRate) {
		generator.generateWave(sample, toneLength, toneFreq, sampleRate);
	}
	
	@Override
	public String toString() {
		return name;
	}

	public static final WaveForm SINE_WAVE = new WaveForm("Sine",
			new Generator() {
				@Override
				public void generateWave(ShortArray sample, int toneLength,
						float toneFreq, int sampleRate) {
					
					short amplitude = Short.MAX_VALUE;
					// Make sure we've enough space for the tone
					sample.ensureCapacity(toneLength);
					
					// Draw the sine wave at the given frequency
					float angle = 0;
					float angular_frequency = (float) (2 * Math.PI) * toneFreq
							/ sampleRate;
					for (int t = 0; t < toneLength; t++) {
						sample.add((short) (amplitude * ((float) Math
								.sin(angle))));
						angle += angular_frequency;
					}
				}
			});

	public static final WaveForm SQUARE_WAVE = new WaveForm("Square",
			new Generator() {
				@Override
				public void generateWave(ShortArray sample, int toneLength,
						float toneFreq, int sampleRate) {

					// Make sure we've enough space for the tone
					sample.ensureCapacity(toneLength);
					
					short amplitude = Short.MAX_VALUE;
					int phaseLen = (int) ((sampleRate / toneFreq) / 2);
					for (int t = 0; t < toneLength; t++) {
						if ((t / phaseLen) % 2 == 0) {
							sample.add(amplitude);
						} else {
							sample.add(-amplitude);
						}
					}
				}
			});

	public static final WaveForm TRIANGLE_WAVE = new WaveForm("Triangle",
			new Generator() {
				@Override
				public void generateWave(ShortArray sample, int toneLength,
						float toneFreq, int sampleRate) {

					// Make sure we've enough space for the tone
					sample.ensureCapacity(toneLength);

					short amplitude = Short.MAX_VALUE;
					int halfCycle = (int) ((sampleRate / toneFreq) / 2);
					for (int t = 0; t < toneLength; t++) {
						int cyclePart = (t / halfCycle) % 2; // 0 and 1
						int phase = t % halfCycle;
						if (cyclePart == 0) {
							sample.add(-amplitude + (2 * amplitude / halfCycle) * phase);
						} else {
							sample.add(3 * amplitude - (2 * amplitude / halfCycle) * phase);
						}
					}
				}
			});

	public static final WaveForm SAWTOOTH_WAVE = new WaveForm("Sawtooth",
			new Generator() {
				@Override
				public void generateWave(ShortArray sample, int toneLength,
						float toneFreq, int sampleRate) {

					// Make sure we've enough space for the tone
					sample.ensureCapacity(toneLength);
					
					short amplitude = Short.MAX_VALUE;
					int cycle = (int) (sampleRate / toneFreq);
					for (int t = 0; t < toneLength; t++) {
						int phase = t % cycle;
						sample.add(amplitude - (2 * amplitude / cycle) * phase);
					}
				}
			});

	public static final WaveForm NOISE_WAVE = new WaveForm("Noise",
			new Generator() {
				@Override
				public void generateWave(ShortArray sample, int toneLength,
						float toneFreq, int sampleRate) {

					// Make sure we've enough space for the tone
					sample.ensureCapacity(toneLength);
					
					Random rand = new Random(System.currentTimeMillis());
					
					short amplitude = Short.MAX_VALUE;
					int phaseLen = (int) ((sampleRate / toneFreq) / 2);
					for (int t = 0; t < toneLength; t++) {
						if ((t / phaseLen) % 2 == 0) {
							sample.add(rand.nextInt(amplitude));
						} else {
							sample.add(-rand.nextInt(amplitude));
						}
					}
				}
			});

	
	public static final WaveForm[] WAVE_FORM_LIST = {
		SINE_WAVE, SQUARE_WAVE, TRIANGLE_WAVE, SAWTOOTH_WAVE, NOISE_WAVE
	};
}
