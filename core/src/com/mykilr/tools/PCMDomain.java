package com.mykilr.tools;

import com.badlogic.gdx.utils.FloatArray;

public class PCMDomain {

	public FloatArray real;
	public FloatArray imaginary;
	public FloatArray magnitude;
	public FloatArray phase;
	public int padding;
	
	public PCMDomain(int capacity) {
		real = new FloatArray();
		real.ensureCapacity(capacity);
		imaginary = new FloatArray();
		imaginary.ensureCapacity(capacity);
		magnitude = new FloatArray();
		magnitude.ensureCapacity(capacity);
		phase = new FloatArray();
		phase.ensureCapacity(capacity);
		padding = 0;
	}

	public PCMDomain() {
		this(0);
	}

	/**
	 * Updates the polar coords in this DFT from the rectangular coords
	 * @return the maximum scale of the magnitude
	 */
	public float updatePolarFromRectangular() {
		float minMa = Float.MAX_VALUE;
		float maxMa = Float.MIN_VALUE;
		
		magnitude.clear();
		phase.clear();
		magnitude.ensureCapacity(real.size);
		phase.ensureCapacity(real.size);
		for (int k = 0; k < real.size; k++) {
			float mag = (float) Math.sqrt(real.get(k) * real.get(k) +
					imaginary.get(k) * imaginary.get(k));
			
			magnitude.add(mag);
			phase.add((float) Math.atan2(imaginary.get(k), real.get(k)));
			
			minMa = (minMa <= mag) ? minMa : mag;
			maxMa = (maxMa > mag) ? maxMa : mag;
		}
		return Math.max(Math.abs(minMa), Math.abs(maxMa)) * 2;
	}
	
	public void updateRectangularFromPolar() {
		real.clear();
		imaginary.clear();
		
		real.ensureCapacity(magnitude.size);
		imaginary.ensureCapacity(magnitude.size);
		for (int k = 0; k < magnitude.size; k++) {
			real.add((float) (magnitude.get(k) * Math.cos(phase.get(k))));
			imaginary.add((float) (magnitude.get(k) * Math.sin(phase.get(k))));
		}
	}

}
