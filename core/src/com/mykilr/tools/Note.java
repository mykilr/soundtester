package com.mykilr.tools;

public class Note {

	public Note(String name, float freq) {
		this.name = name;
		this.freq = freq;
	}
	
	final String name;
	public final float freq; // Hz
	
	@Override
	public String toString() {
		return String.format("%5.2f", freq) + " - " + name;
	};
	
	public static final Note C0 = new Note("C0", 16.35f);
	public static final Note Cs0 = new Note("C#0/Db0", 17.32f);
	public static final Note D0 = new Note("D0", 18.35f);
	public static final Note Ds0 = new Note("D#0/Eb0", 19.45f);
	public static final Note E0 = new Note("E0", 20.60f);
	public static final Note F0 = new Note("F0", 21.83f);
	public static final Note Fs0 = new Note("F#0/Gb0", 23.12f);
	public static final Note G0 = new Note("G0", 24.50f);
	public static final Note Gs0 = new Note("G#0/Ab0", 25.96f);
	public static final Note A0 = new Note("A0", 27.50f);
	public static final Note As0 = new Note("A#0/Bb0", 29.14f);
	public static final Note B0 = new Note("B0", 30.87f);
	public static final Note C1 = new Note("C1", 32.70f);
	public static final Note Cs1 = new Note("C#1/Db1", 34.65f);
	public static final Note D1 = new Note("D1", 36.71f);
	public static final Note Ds1 = new Note("D#1/Eb1", 38.89f);
	public static final Note E1 = new Note("E1", 41.20f);
	public static final Note F1 = new Note("F1", 43.65f);
	public static final Note Fs1 = new Note("F#1/Gb1", 46.25f);
	public static final Note G1 = new Note("G1", 49.00f);
	public static final Note Gs1 = new Note("G#1/Ab1", 51.91f);
	public static final Note A1 = new Note("A1", 55.00f);
	public static final Note As1 = new Note("A#1/Bb1", 58.27f);
	public static final Note B1 = new Note("B1", 61.74f);
	public static final Note C2 = new Note("C2", 65.41f);
	public static final Note Cs2 = new Note("C#2/Db2", 69.30f);
	public static final Note D2 = new Note("D2", 73.42f);
	public static final Note Ds2 = new Note("D#2/Eb2", 77.78f);
	public static final Note E2 = new Note("E2", 82.41f);
	public static final Note F2 = new Note("F2", 87.31f);
	public static final Note Fs2 = new Note("F#2/Gb2", 92.50f);
	public static final Note G2 = new Note("G2", 98.00f);
	public static final Note Gs2 = new Note("G#2/Ab2", 103.83f);
	public static final Note A2 = new Note("A2", 110.00f);
	public static final Note As2 = new Note("A#2/Bb2", 116.54f);
	public static final Note B2 = new Note("B2", 123.47f);
	public static final Note C3 = new Note("C3", 130.81f);
	public static final Note Cs3 = new Note("C#3/Db3", 138.59f);
	public static final Note D3 = new Note("D3", 146.83f);
	public static final Note Ds3 = new Note("D#3/Eb3", 155.56f);
	public static final Note E3 = new Note("E3", 164.81f);
	public static final Note F3 = new Note("F3", 174.61f);
	public static final Note Fs3 = new Note("F#3/Gb3", 185.00f);
	public static final Note G3 = new Note("G3", 196.00f);
	public static final Note Gs3 = new Note("G#3/Ab3", 207.65f);
	public static final Note A3 = new Note("A3", 220.00f);
	public static final Note As3 = new Note("A#3/Bb3", 233.08f);
	public static final Note B3 = new Note("B3", 246.94f);
	public static final Note C4 = new Note("C4", 261.63f);
	public static final Note Cs4 = new Note("C#4/Db4", 277.18f);
	public static final Note D4 = new Note("D4", 293.66f);
	public static final Note Ds4 = new Note("D#4/Eb4", 311.13f);
	public static final Note E4 = new Note("E4", 329.63f);
	public static final Note F4 = new Note("F4", 349.23f);
	public static final Note Fs4 = new Note("F#4/Gb4", 369.99f);
	public static final Note G4 = new Note("G4", 392.00f);
	public static final Note Gs4 = new Note("G#4/Ab4", 415.30f);
	public static final Note A4 = new Note("A4", 440.00f);
	public static final Note As4 = new Note("A#4/Bb4", 466.16f);
	public static final Note B4 = new Note("B4", 493.88f);
	public static final Note C5 = new Note("C5", 523.25f);
	public static final Note Cs5 = new Note("C#5/Db5", 554.37f);
	public static final Note D5 = new Note("D5", 587.33f);
	public static final Note Ds5 = new Note("D#5/Eb5", 622.25f);
	public static final Note E5 = new Note("E5", 659.25f);
	public static final Note F5 = new Note("F5", 698.46f);
	public static final Note Fs5 = new Note("F#5/Gb5", 739.99f);
	public static final Note G5 = new Note("G5", 783.99f);
	public static final Note Gs5 = new Note("G#5/Ab5", 830.61f);
	public static final Note A5 = new Note("A5", 880.00f);
	public static final Note As5 = new Note("A#5/Bb5", 932.33f);
	public static final Note B5 = new Note("B5", 987.77f);
	public static final Note C6 = new Note("C6", 1046.50f);
	public static final Note Cs6 = new Note("C#6/Db6", 1108.73f);
	public static final Note D6 = new Note("D6", 1174.66f);
	public static final Note Ds6 = new Note("D#6/Eb6", 1244.51f);
	public static final Note E6 = new Note("E6", 1318.51f);
	public static final Note F6 = new Note("F6", 1396.91f);
	public static final Note Fs6 = new Note("F#6/Gb6", 1479.98f);
	public static final Note G6 = new Note("G6", 1567.98f);
	public static final Note Gs6 = new Note("G#6/Ab6", 1661.22f);
	public static final Note A6 = new Note("A6", 1760.00f);
	public static final Note As6 = new Note("A#6/Bb6", 1864.66f);
	public static final Note B6 = new Note("B6", 1975.53f);
	public static final Note C7 = new Note("C7", 2093.00f);
	public static final Note Cs7 = new Note("C#7/Db7", 2217.46f);
	public static final Note D7 = new Note("D7", 2349.32f);
	public static final Note Ds7 = new Note("D#7/Eb7", 2489.02f);
	public static final Note E7 = new Note("E7", 2637.02f);
	public static final Note F7 = new Note("F7", 2793.83f);
	public static final Note Fs7 = new Note("F#7/Gb7", 2959.96f);
	public static final Note G7 = new Note("G7", 3135.96f);
	public static final Note Gs7 = new Note("G#7/Ab7", 3322.44f);
	public static final Note A7 = new Note("A7", 3520.00f);
	public static final Note As7 = new Note("A#7/Bb7", 3729.31f);
	public static final Note B7 = new Note("B7", 3951.07f);
	public static final Note C8 = new Note("C8", 4186.01f);
	public static final Note Cs8 = new Note("C#8/Db8", 4434.92f);
	public static final Note D8 = new Note("D8", 4698.63f);
	public static final Note Ds8 = new Note("D#8/Eb8", 4978.03f);
	public static final Note E8 = new Note("E8", 5274.04f);
	public static final Note F8 = new Note("F8", 5587.65f);
	public static final Note Fs8 = new Note("F#8/Gb8", 5919.91f);
	public static final Note G8 = new Note("G8", 6271.93f);
	public static final Note Gs8 = new Note("G#8/Ab8", 6644.88f);
	public static final Note A8 = new Note("A8", 7040.00f);
	public static final Note As8 = new Note("A#8/Bb8", 7458.62f);
	public static final Note B8 = new Note("B8", 7902.13f);
	public static final Note C9 = new Note("C9", 8372.02f);
	public static final Note Cs9 = new Note("C#9/Bb9", 8869.84f);
	public static final Note D9 = new Note("D9", 9397.27f);
	public static final Note Ds9 = new Note("D#9/Eb9", 9956.06f);
	public static final Note E9 = new Note("E9", 10548.08f);
	public static final Note F9 = new Note("F9", 11175.30f);
	public static final Note Fs9 = new Note("F#9/Gb9", 11839.82f);
	public static final Note G9 = new Note("G9", 12543.85f);
	public static final Note Gs9 = new Note("G#9/Ab9", 13289.75f);
	public static final Note A9 = new Note("A9", 14080.00f);
	public static final Note As9 = new Note("A#9/Bb9", 14917.24f);
	public static final Note B9 = new Note("B9", 15804.26f);
	
	// Alias Flats to Sharps
	public static final Note Db0 = Cs0;
	public static final Note Eb0 = Ds0;
	public static final Note Gb0 = Fs0;
	public static final Note Ab0 = Gs0;
	public static final Note Bb0 = As0;
	public static final Note Db1 = Cs1;
	public static final Note Eb1 = Ds1;
	public static final Note Gb1 = Fs1;
	public static final Note Ab1 = Gs1;
	public static final Note Bb1 = As1;
	public static final Note Db2 = Cs2;
	public static final Note Eb2 = Ds2;
	public static final Note Gb2 = Fs2;
	public static final Note Ab2 = Gs2;
	public static final Note Bb2 = As2;
	public static final Note Db3 = Cs3;
	public static final Note Eb3 = Ds3;
	public static final Note Gb3 = Fs3;
	public static final Note Ab3 = Gs3;
	public static final Note Bb3 = As3;
	public static final Note Db4 = Cs4;
	public static final Note Eb4 = Ds4;
	public static final Note Gb4 = Fs4;
	public static final Note Ab4 = Gs4;
	public static final Note Bb4 = As4;
	public static final Note Db5 = Cs5;
	public static final Note Eb5 = Ds5;
	public static final Note Gb5 = Fs5;
	public static final Note Ab5 = Gs5;
	public static final Note Bb5 = As5;
	public static final Note Db6 = Cs6;
	public static final Note Eb6 = Ds6;
	public static final Note Gb6 = Fs6;
	public static final Note Ab6 = Gs6;
	public static final Note Bb6 = As6;
	public static final Note Db7 = Cs7;
	public static final Note Eb7 = Ds7;
	public static final Note Gb7 = Fs7;
	public static final Note Ab7 = Gs7;
	public static final Note Bb7 = As7;
	public static final Note Db8 = Cs8;
	public static final Note Eb8 = Ds8;
	public static final Note Gb8 = Fs8;
	public static final Note Ab8 = Gs8;
	public static final Note Bb8 = As8;
	public static final Note Db9 = Cs9;
	public static final Note Eb9 = Ds9;
	public static final Note Gb9 = Fs9;
	public static final Note Ab9 = Gs9;
	public static final Note Bb9 = As9;
	
	public static final Note[] NOTE_LIST = { 
			C0, Cs0, D0, Ds0, E0, F0, Fs0, G0, Gs0, A0, As0, B0, 
			C1, Cs1, D1, Ds1, E1, F1, Fs1, G1, Gs1, A1, As1, B1, 
			C2, Cs2, D2, Ds2, E2, F2, Fs2, G2, Gs2, A2, As2, B2, 
			C3, Cs3, D3, Ds3, E3, F3, Fs3, G3, Gs3, A3, As3, B3, 
			C4, Cs4, D4, Ds4, E4, F4, Fs4, G4, Gs4, A4, As4, B4, 
			C5, Cs5, D5, Ds5, E5, F5, Fs5, G5, Gs5, A5, As5, B5, 
			C6, Cs6, D6, Ds6, E6, F6, Fs6, G6, Gs6, A6, As6, B6, 
			C7, Cs7, D7, Ds7, E7, F7, Fs7, G7, Gs7, A7, As7, B7, 
			C8, Cs8, D8, Ds8, E8, F8, Fs8, G8, Gs8, A8, As8, B8, 
			C9, Cs9, D9, Ds9, E9, F9, Fs9, G9, Gs9, A9, As9, B9};
	
	public static int getNumNotes() {
		return NOTE_LIST.length;
	}
	
	/**
	 * Scales
	 */
	public static final int[] MAJOR = {2, 2, 1, 2, 2, 2, 1};
	public static final int[] NATURAL_MINOR = {2, 1, 2, 2, 1, 2, 2};
	public static final int[] HARMONIC_MINOR = {2, 1, 2, 2, 1, 3, 1};
	public static final int[] DORIAN_MINOR = {2, 1, 2, 2, 2, 1, 2};
	public static final int[] CHROMATIC = {1};
	public static final int[] WHOLE_TONE = {2};
	public static final int[] OCTATONIC = {1, 2};
	public static final int[] PENTATONIC = {2, 2, 3, 2, 3};
	
	
	/**
	 * Returns the note in the chromatic arrangement nearest to the given frequency
	 * @param freq
	 * @return
	 */
	public static Note nearest(float freq) {
		int i = 0;
		// Increment the index as long as the next Note isn't higher pitched
		while (i + 1 < NOTE_LIST.length && freq > NOTE_LIST[i + 1].freq) {
			i++;
		}
		// If we're not lower than the first note or higher than the last,
		// find the closer of the 2 nearest notes
		if (i + 1 < NOTE_LIST.length) {
			float diff1 = Math.abs(freq - NOTE_LIST[i].freq);
			float diff2 = Math.abs(NOTE_LIST[i + 1].freq - freq);
			i = diff2 > diff1 ? i : i + 1;
		}
		
		return NOTE_LIST[i];
	}
	
	/**
	 * Returns the index of the given note in the chromatic arrangement of C0 to B8
	 * Returns -1 if the note is not found
	 * @param note
	 * @return
	 */
	private static int getNoteIndex(Note note) {
		int i = 0;
		while (i < NOTE_LIST.length && NOTE_LIST[i] != note) {
			i++;
		}
		if (i >= NOTE_LIST.length) {
			i = -1;
		}
		return i;
	}

	/**
	 * Returns an array of ascending Notes starting at the given note and raising
	 * through the given scale until, at most, numNotes are in the array. 
	 * If the number of notes in the scale would pass B8 then the array may be 
	 * shorter than numNotes.
	 * @param startNote
	 * @param numNotes
	 * @param scale
	 * @return
	 */
	public static Note[] getNotesInScale(Note startNote, int numNotes, int[] scale) {
		int start = getNoteIndex(startNote);
		
		int breadth = 0;
		int len = 0;
		int scaleInd = 0;
		while (breadth + start + scale[scaleInd] < NOTE_LIST.length && len < numNotes) {
			breadth += scale[scaleInd];
			len++;
			scaleInd = (scaleInd + 1) % scale.length;
		}
		
		Note[] notes = new Note[len];
		int noteInd = 0;
		int chromInd = start;
		scaleInd = 0;
		while (noteInd < notes.length) {
			notes[noteInd] = NOTE_LIST[chromInd];
			noteInd++;
			chromInd += scale[scaleInd];
			scaleInd = (scaleInd + 1) % scale.length;
		}
		
		return notes;
	}
}
