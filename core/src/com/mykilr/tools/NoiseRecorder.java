package com.mykilr.tools;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.AudioRecorder;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.ShortArray;

public class NoiseRecorder implements Runnable {
	
	private class RecorderListener extends ClickListener {
		private NoiseRecorder recorder;
		private Thread recordThread;
		
		RecorderListener(NoiseRecorder recorder) {
			this.recorder = recorder;
		}
		
		@Override
		public void clicked(InputEvent event, float x, float y) {
			super.clicked(event, x, y);
			if (!recorder.stop) {
				recorder.stop();
				try {
					recordThread.join();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				recordThread = null;
			} else {
				recordThread = new Thread(recorder);
				recordThread.start();
			}
		}
	}

	private static final int PLAY_SAMPLES = 8096;
	private int sampleRate;
	private AudioRecorder audioRec;
	boolean stop;
	private ShortArray buffer;
	private RecorderListener listener;
	private TextButton recordButton;
	private String recordText = "";
	private String stopText = "";
	
	public NoiseRecorder (ShortArray dataContainer, int sampleRate, boolean isMono) {
		audioRec = Gdx.audio.newAudioRecorder(sampleRate, isMono);
		stop = true;
		this.sampleRate = sampleRate;
		this.buffer = dataContainer;
		this.listener = new RecorderListener(this);
	}

	@Override
	public void run() {
		// Clear dataContainer and ensure 1 second capacity
		buffer.clear();
		short[] clipBuffer = new short[PLAY_SAMPLES];
		buffer.ensureCapacity(sampleRate * 20);
		stop = false;
		// Will probably have a record button, but just in case
		if (recordButton != null) {
			recordButton.setText(stopText);
		}
		// Try to record in a non-blocking manner
		while (!stop) {
			// Gdx.app.log("Rec", "Recording. Offset: " + offset + ", buffer.size: " + buffer.size);
			audioRec.read(clipBuffer, 0, PLAY_SAMPLES);
			buffer.ensureCapacity(sampleRate);
			buffer.addAll(clipBuffer, 0, PLAY_SAMPLES);
		}
		if (recordButton != null) {
			recordButton.setText(recordText);
		}
	}
	
	public TextButton getRecordStopButton(String recordText, String stopText, Skin skin) {
		this.recordText = recordText;
		this.stopText = stopText;
		if (recordButton != null) {
			recordButton.setText(stop == true ? recordText : stopText);
		} else {
			recordButton = new TextButton(stop == true ? recordText : stopText, skin);
			recordButton.addListener(listener);
		}
		return recordButton;
	}
	
	public void stop() {
		stop = true;
	}

	public void dispose() {
		audioRec.dispose();
	}

}
