package com.mykilr.tools;

import com.badlogic.gdx.utils.ShortArray;

public class PCMData {
	
	private static final int DEFAULT_SAMPLE_RATE = 44100;
	private static final int MONO = 1;

	public ShortArray sample;
	public int sampleRate;
	public short channels;

	public PCMData() {
		this(new ShortArray());
	}
	
	public PCMData(ShortArray shortArray) {
		this.sample = shortArray;
		this.sampleRate = DEFAULT_SAMPLE_RATE; // Default
		this.channels = MONO; // Default
	}
	
	public void set(PCMData data) {
		this.sample.clear();
		this.sample.ensureCapacity(data.sample.size);
		this.sample.addAll(data.sample);
		this.sampleRate = data.sampleRate;
		this.channels = data.channels;
	}

}