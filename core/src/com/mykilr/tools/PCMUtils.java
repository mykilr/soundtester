package com.mykilr.tools;

import java.io.PrintStream;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.ShortArray;

public class PCMUtils {
	
	static final int EXP_BITS_PER_SAMPLE = 16;
	
	private static final int CHUNK_ID_POS = 0;
	private static final int CHUNK_SIZE_POS = 4;
	private static final int FORMAT_POS = 8;
	private static final int SUBCHUNK_1_ID_POS = 12;
	private static final int SUBCHUNK_1_SIZE_POS = 16;
	private static final int AUDIO_FORMAT_POS = 20;
	private static final int NUM_CHANNEL_POS = 22;
	private static final int SAMPLE_RATE_POS = 24;
	private static final int BYTE_RATE_POS = 28;
	private static final int BLOCK_ALIGN_POS = 32;
	private static final int BITS_PER_SAMPLE_POS = 34;
	private static final int SUBCHUNK_2_ID_POS = 36;
	private static final int SUBCHUNK_2_SIZE_POS = 40;
	private static final int WAV_HEADER_SIZE = 44;		

	static void displaySampleData(PrintStream out, short[] sample) {
		for (int i = 0; i < sample.length; i++) {
			out.println(sample[i]);
		}
	}	
	
	public static void displaySampleData(PrintStream out, ShortArray sample) {
		for (int i = 0; i < sample.size; i++) {
			out.println(sample.get(i));
		}
	}

	public static void loadPCMFromWav(FileHandle externalFile, PCMData data) {
		byte[] header = new byte[WAV_HEADER_SIZE];
		externalFile.readBytes(header, 0, header.length);
		
		// Check, and were appropriate store, the standard header information
		String chunkId = new String(header, CHUNK_ID_POS, 4);
		if (!chunkId.equals("RIFF")) {
			throw new IllegalArgumentException("File header incorrect, expected 'RIFF' got '" + chunkId + "'.");
		}
		
		// Should be the size of the rest of this file, check with subchunk sizes later
		int chunkSize =
				(header[CHUNK_SIZE_POS] & 0xFF) |
				(header[CHUNK_SIZE_POS + 1] & 0xFF) << 8 |
				(header[CHUNK_SIZE_POS + 2] & 0xFF) << 16 |
				(header[CHUNK_SIZE_POS + 3] & 0xFF) << 24;
		
		String format = new String(header, FORMAT_POS, 4);
		if (!format.equals("WAVE")) {
			throw new IllegalArgumentException("File format incorrect, expected 'WAVE' got '" + format + "'.");
		}
		
		String subchunk1id = new String(header, SUBCHUNK_1_ID_POS, 4);
		if (!subchunk1id.equals("fmt ")) {
			throw new IllegalArgumentException("File header incorrect, expected 'fmt ' got '" + subchunk1id + "'.");
		}
		
		int subchunk1size =
				(header[SUBCHUNK_1_SIZE_POS] & 0xFF) |
				(header[SUBCHUNK_1_SIZE_POS + 1] & 0xFF) << 8 |
				(header[SUBCHUNK_1_SIZE_POS + 2] & 0xFF) << 16 |
				(header[SUBCHUNK_1_SIZE_POS + 3] & 0xFF) << 24;
				
		short audioFormat = (short) (
				(header[AUDIO_FORMAT_POS] & 0xFF) |
				(header[AUDIO_FORMAT_POS + 1] & 0xFF) << 8);
		if (audioFormat != 1) {
			throw new IllegalArgumentException("PCM Format " + audioFormat + " not supported, only linear quantization (1) supported.");
		}
	
		data.channels = (short) (
				(header[NUM_CHANNEL_POS] & 0xFF) |
				(header[NUM_CHANNEL_POS + 1] & 0xFF) << 8);
		if (data.channels != 1) {
			throw new IllegalArgumentException("Only mono PCM format currently supported, " + data.channels + " channels in file.");
		}
		
		data.sampleRate = 
				(header[SAMPLE_RATE_POS] & 0xFF) |
				(header[SAMPLE_RATE_POS + 1] & 0xFF) << 8 |
				(header[SAMPLE_RATE_POS + 2] & 0xFF) << 16 |
				(header[SAMPLE_RATE_POS + 3] & 0xFF) << 24;
		
		int byteRate = 
				(header[BYTE_RATE_POS] & 0xFF) |
				(header[BYTE_RATE_POS + 1] & 0xFF) << 8 |
				(header[BYTE_RATE_POS + 2] & 0xFF) << 16 |
				(header[BYTE_RATE_POS + 3] & 0xFF) << 24;
	
		short blockAlign = (short) (
				(header[BLOCK_ALIGN_POS] & 0xFF) |
				(header[BLOCK_ALIGN_POS + 1] & 0xFF) << 8);
	
		short bitsPerSample = (short) (
				(header[BITS_PER_SAMPLE_POS] & 0xFF) |
				(header[BITS_PER_SAMPLE_POS + 1] & 0xFF) << 8);
		if (bitsPerSample != EXP_BITS_PER_SAMPLE) {
			// Not 2 bytes per sample!
			throw new IllegalArgumentException("Expected bits per sample of " + EXP_BITS_PER_SAMPLE + ", got " + bitsPerSample + ".");
		}
		
		short expBlockAlign = (short) (data.channels * bitsPerSample / 8);
		if (blockAlign != expBlockAlign) {
			throw new IllegalArgumentException("Expected block align of " + expBlockAlign + ", got " + blockAlign + ".");
		}
		
		// Check data rates all add up
		int bytesPerSample = bitsPerSample / 8;
		int expByteRate = bytesPerSample * data.sampleRate * data.channels;
		if (byteRate != expByteRate) {
			throw new IllegalArgumentException("Expected byte rate of " + expByteRate + ", got " + byteRate + ".");
		}
		
		String subchunk2id = new String(header, SUBCHUNK_2_ID_POS, 4);
		if (!subchunk2id.equals("data")) {
			throw new IllegalArgumentException("File header incorrect, expected 'data' got '" + subchunk2id + "'.");
		}
		
		int subchunk2size =
				(header[SUBCHUNK_2_SIZE_POS] & 0xFF) |
				(header[SUBCHUNK_2_SIZE_POS + 1] & 0xFF) << 8 |
				(header[SUBCHUNK_2_SIZE_POS + 2] & 0xFF) << 16 |
				(header[SUBCHUNK_2_SIZE_POS + 3] & 0xFF) << 24;
		
		// Check chunk sizes add up
		int expChunkSize = 4 + (8 + subchunk1size) + (8 + subchunk2size);
		if (chunkSize != expChunkSize) {
			throw new IllegalArgumentException("Expected chunk size of " + expChunkSize + ", got " + chunkSize + ".");
		}
		
		// Work out the number of actual sample to expect
		int numSamples = subchunk2size / (data.channels * bytesPerSample);
		data.sample.clear();
		data.sample.ensureCapacity(numSamples);
		
		Gdx.app.log("PCMD", "ChunkID       : " + chunkId);
		Gdx.app.log("PCMD", "ChunkSize     : " + chunkSize);
		Gdx.app.log("PCMD", "Format        : " + format);
		Gdx.app.log("PCMD", "Subchunk1ID   : " + subchunk1id);
		Gdx.app.log("PCMD", "Subchunk1Size : " + subchunk1size);
		Gdx.app.log("PCMD", "AudioFormat   : " + audioFormat);
		Gdx.app.log("PCMD", "NumChannels   : " + data.channels);
		Gdx.app.log("PCMD", "SampleRate    : " + data.sampleRate);
		Gdx.app.log("PCMD", "ByteRate      : " + byteRate);
		Gdx.app.log("PCMD", "BlockAlign    : " + blockAlign);
		Gdx.app.log("PCMD", "BitsPerSample : " + bitsPerSample);
		Gdx.app.log("PCMD", "Subchunk2ID   : " + subchunk2id);
		Gdx.app.log("PCMD", "Subchunk2Size : " + subchunk2size);
		
		byte[] byteData = externalFile.readBytes();
		for (int i = 0; i < numSamples; i++) {
			// Can't imagine this is too efficient - may need to revisit
			int pos = WAV_HEADER_SIZE + i * bytesPerSample;
			data.sample.add((short)(byteData[pos] & 0xFF | ((byteData[pos + 1] &0xFF ) << 8)));
			
			if (i < 50) {
				Gdx.app.log("PCMD", "sample(" + i + "):" + data.sample.get(i));		
			}
		}
	}

	public static void savePCMToWav(PCMData data, FileHandle externalFile) {
		if (data == null || externalFile == null) { 
			Gdx.app.error("PCMData", "Attempt to save but " + (data == null ? "data" : "file") + " is null");
			return;
		}
		
		short blockAlign = (short) (data.channels * (EXP_BITS_PER_SAMPLE / 8));
		int subchunk1size = 16;
		int subchunk2size = data.sample.size * blockAlign;
		int chunkSize = 4 + (8 + subchunk1size) + (8 + subchunk2size);
		int byteRate = data.sampleRate * blockAlign;
		
		// Create and write Header
		byte[] header = new byte[WAV_HEADER_SIZE];
		header[CHUNK_ID_POS + 0]        = 'R'; // RIFF
		header[CHUNK_ID_POS + 1]        = 'I';
		header[CHUNK_ID_POS + 2]        = 'F';
		header[CHUNK_ID_POS + 3]        = 'F';
		header[CHUNK_SIZE_POS + 0]      = (byte) (chunkSize       & 0xFF);
		header[CHUNK_SIZE_POS + 1]      = (byte) (chunkSize >>  8 & 0xFF);
		header[CHUNK_SIZE_POS + 2]      = (byte) (chunkSize >> 16 & 0xFF);
		header[CHUNK_SIZE_POS + 3]      = (byte) (chunkSize >> 24 & 0xFF);
		header[FORMAT_POS + 0]          = 'W'; // WAVE
		header[FORMAT_POS + 1]          = 'A';
		header[FORMAT_POS + 2]          = 'V';
		header[FORMAT_POS + 3]          = 'E';
		header[SUBCHUNK_1_ID_POS + 0]   = 'f'; // 'fmt '
		header[SUBCHUNK_1_ID_POS + 1]   = 'm';
		header[SUBCHUNK_1_ID_POS + 2]   = 't';
		header[SUBCHUNK_1_ID_POS + 3]   = ' ';
		header[SUBCHUNK_1_SIZE_POS + 0] = (byte) (subchunk1size       & 0xFF);
		header[SUBCHUNK_1_SIZE_POS + 1] = (byte) (subchunk1size >>  8 & 0xFF);
		header[SUBCHUNK_1_SIZE_POS + 2] = (byte) (subchunk1size >> 16 & 0xFF);
		header[SUBCHUNK_1_SIZE_POS + 3] = (byte) (subchunk1size >> 24 & 0xFF);
		header[AUDIO_FORMAT_POS + 0]    = 0x01; // No compression
		header[AUDIO_FORMAT_POS + 1]    = 0x00;
		header[NUM_CHANNEL_POS + 0]     = (byte) (data.channels       & 0xFF);
		header[NUM_CHANNEL_POS + 1]     = (byte) (data.channels >>  8 & 0xFF);
		header[SAMPLE_RATE_POS + 0]     = (byte) (data.sampleRate       & 0xFF);
		header[SAMPLE_RATE_POS + 1]     = (byte) (data.sampleRate >>  8 & 0xFF);
		header[SAMPLE_RATE_POS + 2]     = (byte) (data.sampleRate >> 16 & 0xFF);
		header[SAMPLE_RATE_POS + 3]     = (byte) (data.sampleRate >> 24 & 0xFF);
		header[BYTE_RATE_POS + 0]       = (byte) (byteRate       & 0xFF);
		header[BYTE_RATE_POS + 1]       = (byte) (byteRate >>  8 & 0xFF);
		header[BYTE_RATE_POS + 2]       = (byte) (byteRate >> 16 & 0xFF);
		header[BYTE_RATE_POS + 3]       = (byte) (byteRate >> 24 & 0xFF);
		header[BLOCK_ALIGN_POS + 0]     = (byte) (blockAlign      & 0xFF);
		header[BLOCK_ALIGN_POS + 1]     = (byte) (blockAlign >> 8 & 0xFF); 
		header[BITS_PER_SAMPLE_POS + 0] = (byte) (EXP_BITS_PER_SAMPLE      & 0xFF);
		header[BITS_PER_SAMPLE_POS + 1] = (byte) (EXP_BITS_PER_SAMPLE >> 8 & 0xFF);
		header[SUBCHUNK_2_ID_POS + 0]   = 'd'; // data
		header[SUBCHUNK_2_ID_POS + 1]   = 'a';
		header[SUBCHUNK_2_ID_POS + 2]   = 't';
		header[SUBCHUNK_2_ID_POS + 3]   = 'a';
		header[SUBCHUNK_2_SIZE_POS + 0] = (byte) (subchunk2size       & 0xFF);
		header[SUBCHUNK_2_SIZE_POS + 1] = (byte) (subchunk2size >>  8 & 0xFF);
		header[SUBCHUNK_2_SIZE_POS + 2] = (byte) (subchunk2size >> 16 & 0xFF);
		header[SUBCHUNK_2_SIZE_POS + 3] = (byte) (subchunk2size >> 24 & 0xFF);
			
		externalFile.writeBytes(header, false);
		
		// Align and append data
		byte[] byteData = new byte[subchunk2size];
		for (int i = 0; i < data.sample.size; i++) {
			// Can't imagine this is too efficient - may need to revisit
			short datum = data.sample.get(i);
			byteData[i * 2]     = (byte) (datum      & 0xFF);
			byteData[i * 2 + 1] = (byte) (datum >> 8 & 0xFF);
		}
		
		externalFile.writeBytes(byteData, true);
	}

	public static void savePCMToWav(PCMData data, String wavFile) {
		FileHandle externalFile = Gdx.files.external(wavFile);
		
		savePCMToWav(data, externalFile);
	}

	public static void loadPCMFromWav(String wavFile, PCMData data) throws IllegalArgumentException {
		FileHandle externalFile = Gdx.files.external(wavFile);
		
		loadPCMFromWav(externalFile, data);
	}

	static public PCMData loadPCMFromWav(String wavFile) {
		PCMData data = new PCMData(new ShortArray());
		loadPCMFromWav(wavFile, data);
		return data;
	}

}
